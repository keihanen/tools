PROGRAM stdmap

! Compute the std of a Healpix map.
! If two file names are given, compute the std of the difference.
! Optionally, write out the difference map, or apply a mask.
! Column index may be given, if not, read all columns.
! Usage: stdmap file1.fits [file2.fits -out diff.fits -mask mask.fits -col i]'

   use planck_types
   use planck_config
   use fitsmod2

   implicit none
   integer              :: i, n, k, ibase, imap, npix
   integer              :: nopar, ipar, icol, icol_out
   integer              :: ncols1, ncols2, nside1, nside2, nside_mask
   integer(i8b)         :: offset, off, chunksize, nout
   character(len=200)   :: file1, file2, file_out, file_mask, cpar
   character(len=40)    :: ordering1, ordering2, ordering_mask
   logical              :: found
   real(sp),allocatable :: map1(:), map2(:), mask(:), smap(:)
   real(dp)             :: dsum, dsum2, stdm, dmap, ave, minm, maxm
   logical              :: two_files, use_mask, write_out
   real(sp),parameter   :: Healpix_undef = -1.6375e30
   type(fitshandle)     :: handle1, handle2, hout, hmask
   type(fitscolumn),allocatable :: columns(:)

   nopar = command_argument_count()
   if (nopar==0) then
      write(*,*) 'Usage: stdmap file1.fits ', &
                 '[file2.fits -out diff.fits -mask mask.fits -col i]'
      stop
   endif

   file1 = ''
   file2 = ''
   file_mask = ''
   file_out = ''
   icol = -1

   ipar = 0
   do
      ipar = ipar+1
      if (ipar.gt.nopar) exit

      call get_command_argument(ipar,cpar)

      if (cpar=='-out') then
         ipar = ipar+1
         call get_command_argument(ipar,file_out)
      elseif (cpar=='-mask') then
         ipar = ipar+1
         call get_command_argument(ipar,file_mask)
      elseif (cpar=='-col') then
         ipar = ipar+1
         call get_command_argument(ipar,cpar)
         read(cpar,*) icol
      elseif (ipar==1) then
         call get_command_argument(1,file1)
      elseif (ipar==2) then
         call get_command_argument(2,file2)
      else
         write(*,*) 'Unrecognized option: ',trim(cpar)
         write(*,*) 'Usage: ./stdmap file1.fits [file2.fits -out diff.fits'// &
                    ' -mask mask.fits -col i]'
         stop
      endif
   enddo

   two_files = (len_trim(file2).gt.0)
   write_out = (len_trim(file_out).gt.0)
   use_mask = (len_trim(file_mask).gt.0)

   inquire(file=file1,exist=found)
   if (.not.found) then
      write(*,*) 'File not found: ',trim(file1)
      call exit_with_status(1)
   endif

   if (two_files) then
      inquire(file=file2,exist=found)
      if (.not.found) then
         write(*,*) 'File not found: ',trim(file2)
         call exit_with_status(1)
      endif
   endif

   if (use_mask) then
      inquire(file=file_mask,exist=found)
      if (.not.found) then
         write(*,*) 'File not found: ',trim(file_mask)
         call exit_with_status(1)
      endif
   endif

!Open files
   call fits_open(handle1,file1)

   call fits_get_key(handle1,'nside',nside1)
   call fits_get_key(handle1,'ordering',ordering1)
   ncols1 = size(handle1%columns)

   write(*,*)
   write(*,*)          'Map 1: ',trim(file1)
   write(*,'(x,a,i8)') 'nside    = ',nside1
   write(*,*)          'ordering = '//trim(ordering1)
   write(*,'(x,a,i8)') 'ncols    = ',ncols1
   call lowercase(ordering1)

   if (two_files) then
      call fits_open(handle2,file2)

      call fits_get_key(handle2,'nside',nside2)
      call fits_get_key(handle2,'ordering',ordering2)
      ncols2 = size(handle2%columns)

      write(*,*)
      write(*,*)          'Map 2: ',trim(file2)
      write(*,'(x,a,i8)') 'nside    = ',nside2
      write(*,*)          'ordering = '//trim(ordering2)
      write(*,'(x,a,i8)') 'ncols    = ',ncols2
      call lowercase(ordering2)

      if (nside1.ne.nside2) then
         call exit_with_status(1, 'Error: nside1<>nside2')
      elseif (ncols1.ne.ncols2.and.icol.lt.0) then
         call exit_with_status(1, 'Error: ncols1<>ncols2')
      elseif (ordering1.ne.ordering2) then
         call exit_with_status(1, 'Error: ordering1<>ordering2')
      endif
   endif

   if (use_mask) then
      call fits_open(hmask,file_mask)

      call fits_get_key(hmask,'nside',nside_mask)
      call fits_get_key(hmask,'ordering',ordering_mask)

      write(*,*)
      write(*,*)          'Mask: ',trim(file_mask)
      write(*,'(x,a,i8)') 'nside    = ',nside_mask
      write(*,*)          'ordering = '//trim(ordering_mask)
      call lowercase(ordering_mask)

      if (nside_mask.ne.nside1) then
         call exit_with_status(1, 'Error: nside does not match')
      elseif (ordering_mask.ne.ordering1) then
         call exit_with_status(1, 'Error: ordering does not match')
      endif
   endif

   npix = nside1*nside1

   allocate(map1(npix))
   map1 = 0.0

   if (two_files) then
      allocate(map2(npix))
      map2 = 0.0
   endif

   if (use_mask) then
      allocate(mask(npix))
      mask = 0.0
   endif

   if (write_out) then
      allocate(smap(npix))
      smap = 0.0
   endif

! Write out the difference map
   if (write_out) then
      call fits_create(hout,trim(file_out))

      if (icol.le.0) then
         allocate(columns(ncols1))
         do i = 1,ncols1
            columns(i) = handle1%columns(i)
            columns(i)%repcount = 1
         enddo
      else
         allocate(columns(1))
         columns(1) = handle1%columns(icol)
         columns(1)%repcount = 1
      endif

      call fits_insert_bintab(hout,columns)
      deallocate(columns)

      call fits_add_key(hout,'PIXTYPE', 'HEALPIX','HEALPIX Pixelisation')
      call fits_add_key(hout,'ORDERING',ordering1, 'Pixel ordering scheme')
      call fits_add_key(hout,'NSIDE',nside1,'Resolution parameter for HEALPIX')
      call fits_add_key(hout,'FIRSTPIX',0,'First pixel # (0 based)')
      call fits_add_key(hout,'LASTPIX',12*nside1**2-1,'Last pixel # (0 based)')
      if (size(columns)==3) call fits_add_key(hout,'POLAR','Polarization flag')
   endif

!Read input map(s) and compute the std

   icol_out = 0
   do imap = 1,ncols1

      if (icol.ge.0.and.icol.ne.imap) cycle

      icol_out = icol_out+1
      if (write_out) chunksize = fits_fast_nelms(hout,icol_out)

      dsum = 0.0
      dsum2 = 0.0

      ave = 0.0
      stdm = 0.0
      minm = 1.e30
      maxm = -1.e30

      n = 0
      offset = 0
      do ibase = 1,12   !Read in pieces to save memory

         dmap = 0.0

         call fits_read_column(handle1,imap,map1,offset)

         if (two_files) call fits_read_column(handle2,imap,map2,offset)
         if (use_mask) call fits_read_column(hmask,1,mask,offset)

         do i = 1,npix

            if (two_files) then
               if (map1(i)==Healpix_undef.or.map2(i)==Healpix_undef) then
                  dmap = Healpix_undef
               else
                  dmap = map1(i)-map2(i)
               endif
            else
               if (map1(i)==Healpix_undef) then
                  dmap = Healpix_undef
               else
                  dmap = map1(i)
               endif

            endif

            if (use_mask) then
               if (mask(i).le.0) dmap=Healpix_undef
            endif

            if (write_out) smap(i)=dmap

            if (dmap.gt.-1e30) then
               dsum = dsum+dmap
               dsum2 = dsum2 +dmap*dmap
               n = n+1

               if (dmap.lt.minm) minm = dmap
               if (dmap.gt.maxm) maxm = dmap
            endif
         enddo

         if (write_out) then
            off = 0
            do while (off.lt.npix)
               nout = min(chunksize,npix-off)
               call fits_write_column(hout,icol_out,smap(off+1:off+nout),offset+off)
               off = off+nout
            enddo
         endif
              
         offset = offset+npix
      enddo

      write(*,*)
      write(*,'(a,i4,3x,a)') ' Column = ',imap,trim(handle1%columns(imap)%name)
      write(*,*) 'Pixels ',n
      write(*,*) 'Sky coverage ',n/(12.*npix)*100.

      if (n.gt.1) then
         stdm = dsum2-dsum*dsum/n
         stdm = sqrt(stdm/(n-1))
      endif

      if (n.gt.0) ave = dsum/n

      if (abs(ave).lt.100.and.abs(ave).gt.1) then
         write(*,'(" mean =",f14.6)') ave
      else
         write(*,'(" mean =",es14.6)') ave
      endif

      if (abs(minm).lt.100.and.abs(minm).gt.1) then
         write(*,'(" min  =",f14.6)') minm
      else
         write(*,'(" min  =",es14.6)') minm
      endif

      if (abs(maxm).lt.100.and.abs(maxm).gt.1) then
         write(*,'(" max  =",f14.6)') maxm
      else
         write(*,'(" max  =",es14.6)') maxm
      endif

      if (abs(stdm).lt.100.and.abs(stdm).gt.1) then
         write(*,'(" std  =",f14.6)') stdm
      else
         write(*,'(" std  =",es14.6)') stdm
      endif

   enddo

   call fits_close(handle1)
   if (two_files) call fits_close(handle2)
   if (use_mask)  call fits_close(hmask)
   if (write_out) call fits_close(hout)

   if (two_files) deallocate(map2)
   if (use_mask)  deallocate(mask)
   if (write_out) deallocate(smap)

CONTAINS
   
   SUBROUTINE lowercase(string)

      character(len=*) :: string
      integer :: k, i, i1, i2, n 

      i1 = ichar('A')
      i2 = ichar('Z')
      n = ichar('a')-ichar('A')
 
      do k = 1,len_trim(string)
         i = ichar(string(k:k))

         if (i.ge.i1.and.i.le.i2) i=i+n

         string(k:k) = char(i)
      enddo

   END SUBROUTINE

END PROGRAM
