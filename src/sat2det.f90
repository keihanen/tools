PROGRAM satconv

!Convert satellite pointing to detector pointing

   use planck_config
   use fitsmod2
   use satellite

   implicit none
   integer              :: nopar, ipar
   integer              :: ifirst, ilast, i, k, n, nleft, nosamples, trtype
   integer(i8b)         :: offset
   real(dp)             :: theta_uv, phi_uv, psi_uv, opening, fsample
   real(dp)             :: starttime, endtime, time, shift
   logical              :: rounding, to_galactic, found, kperiods, ktime

   integer              :: nbuffer
   real(dp),allocatable :: theta(:),phi(:),psi(:)

   character(len=80)    :: cpar, key, file_satellite, outfile
   type(fitshandle)     :: outhandle
   type(fitscolumn)     :: columns(3)

   file_satellite  = ''
   outfile = ''
   rounding    = .false.
   to_galactic = .false.
   shift = 0.0

   ifirst = -1
   ilast  = -1
   time   = -1.
   nosamples = -1

   theta_uv = -1000.
   phi_uv   = -1000.
   psi_uv   = -1000.
   opening  =  85.d0
   fsample  = -1.

   kperiods = .false.
   ktime    = .false.

   nopar = command_argument_count()

   if (nopar==0) then
      write(*,*)
      write(*,*) 'Convert satellite pointing to detector pointing'
      write(*,*) 'Usage:'
      write(*,*) 'sat2det -in satpoint.fits -out detpoint.fits'
      write(*,*) '        -fsample -theta x -phi y -psi z [-opening a]'
      write(*,*) '       [-time t -nosamples n]'
      write(*,*) '       [-first n1 -last n2]'
      write(*,*) '       [-rounding T/F  -to_galactic T/F -shift s]'
      write(*,*)
      write(*,*) 'Fsample (Hz) is the sampling frequency.'
      write(*,*) 'Theta,phi,psi (deg) define detector position on focal plane.'
      write(*,*) 'Opening is the opening angle, default 85.0 deg.'
      write(*,*) 'To define the time interval, provide either first and last'
      write(*,*) '  pointing period index (first,last) (starting from 0) or'
      write(*,*) '  starting time (s) and number of samples (time,nosamples).'
      write(*,*) 'Other options:'
      write(*,*) '-rounding:    Round start/end times of pointing periods to', &
                 ' nearest integer second. Default=F.'
      write(*,*) '-to_galactic: Transform from ecliptic to galactic ', &
                 'coordinates. Default=F.'
      write(*,*) '-shift:       Pointing shift as a fraction of sample. ', &
                 'Default=0.'
      write(*,*)
      stop
   endif

   ipar = 0
   do
      ipar = ipar+1
      if (ipar.gt.nopar) exit

      call get_command_argument(ipar,key)

      if (key(1:6)=='-round') then
         rounding = .true.
         cycle
      elseif (key(1:7)=='-to_gal') then
         to_galactic = .true.
         cycle
      elseif (ipar==nopar) then
         write(*,*) 'Error in arguments'
         stop
      endif

      ipar = ipar+1
      call get_command_argument(ipar,cpar)

      if (key(1:3)=='-in') then
         file_satellite = cpar
      elseif (key(1:4)=='-out') then
         outfile = cpar
      elseif (key=='-first') then
         read(cpar,*) ifirst
         kperiods = .true.
      elseif (key=='-last') then
         read(cpar,*) ilast
         kperiods = .true.
      elseif (key(1:5)=='-time') then
         read(cpar,*) time
         ktime = .true.
      elseif (key(1:2)=='-n') then
         read(cpar,*) nosamples
         ktime = .true.
      elseif (key(1:6)=='-theta') then
         read(cpar,*) theta_uv
      elseif (key(1:4)=='-phi') then
         read(cpar,*) phi_uv
      elseif (key(1:4)=='-psi') then
         read(cpar,*) psi_uv
      elseif (key=='-fsample') then
         read(cpar,*) fsample
      elseif (key(1:3)=='-op') then
         read(cpar,*) opening
      elseif (key=='-shift') then
         read(cpar,*) shift
      else
         write(*,*) 'Unrecognized option: ',trim(key)
         stop
      endif
   enddo

   if (kperiods.and.ktime) then
      write(*,*) 'Error in arguments:'
      write(*,*) 'User should provide either (first,last) or ', &
                 '(time,nosamples), but not both.'
      stop
   elseif (.not.kperiods.and..not.ktime) then
      ktime = .true.
      starttime = 0.0
   endif

   write(*,*) 'file_satellite =  ',trim(file_satellite)
   write(*,*) 'outfile        =  ',trim(outfile)
   write(*,'(x,a,f10.3," Hz ")') 'fsample        =',fsample
   write(*,'(x,a,f10.3," deg")') 'theta_uv       =',theta_uv
   write(*,'(x,a,f10.3," deg")') 'phi_uv         =',phi_uv
   write(*,'(x,a,f10.3," deg")') 'psi_uv         =',psi_uv
   write(*,'(x,a,f10.3," deg")') 'opening        =',opening

   if (kperiods) then
      write(*,'(x,a,i8)') 'first period   = ',ifirst
      write(*,'(x,a,i8)') 'last period    = ',ilast
   elseif (ktime) then
      write(*,'(x,a,f14.3," s")') 'starting time  =',starttime
      write(*,'(x,a,i14)')        'nosamples      =',nosamples
   endif
   write(*,'(x,a,l8)')   'rounding       = ',rounding
   write(*,'(x,a,l8)')   'to_galactic    = ',to_galactic
   write(*,'(x,a,f8.3)') 'shift          = ',shift
   write(*,*)

   if (to_galactic) then
      trtype = 5
   else
      trtype = 0
   endif

   found = .true.
   if (fsample.lt.0) then
      write(*,*) 'fsample missing'
      found = .false.
   endif
   if (theta_uv.lt.-999) then
      write(*,*) 'theta missing'
      found = .false.
   endif
   if (phi_uv.lt.-999) then
      write(*,*) 'phi missing'
      found = .false.
   endif
   if (psi_uv.lt.-999) then
      write(*,*) 'psi missing'
      found = .false.
   endif
   if (len_trim(outfile)==0) then
      write(*,*) 'Output file name missing.'
      found = .false.
   endif

   if (.not.found) stop

   inquire(file=file_satellite,exist=found)
   if (.not.found) then
      write(*,*) 'Satellite pointing file not found.'
      stop
   endif

   call read_pointing_periods(file_satellite)

   write(*,'(i6,a)') noperiods,' pointing periods read'

   if (kperiods) then
      if (ilast.lt.0) ilast=noperiods-1

      if (ilast.ge.noperiods) then
         write(*,*) 'Error in arguments: last>=noperiods'
         write(*,*) ilast,noperiods
         stop
      elseif (ifirst.gt.ilast) then
         write(*,*) 'Error in arguments: first>last'
         write(*,*) ifirst,ilast
         stop
      endif

      starttime = starttime_period(ifirst+1)
      endtime   = endtime_period(ilast+1)
      nosamples = (endtime-starttime)*fsample

   elseif (ktime) then
      if (starttime.lt.0) starttime=0.0

      if (nosamples.lt.0) then
         endtime = endtime_period(noperiods)
         nosamples = (endtime-starttime)*fsample
      else
         endtime = starttime +nosamples/fsample
      endif
   endif

   write(*,'(x,a,f14.3)') 'starting time  =',starttime
   write(*,'(x,a,f14.3)') 'ending time    =',endtime
   write(*,'(x,a,i14)')   'nosamples      =',nosamples

   call select_detector_satpoint(theta_uv,phi_uv,psi_uv,opening,fsample,unit='deg')

   call read_satellite_position(starttime,endtime,file_satellite,trtype)

   nbuffer = 1e6
   allocate(theta(nbuffer))
   allocate(phi(nbuffer))
   allocate(psi(nbuffer))

   call fits_create(outhandle,"!"//outfile)
   do i = 1,3
      columns(i)%type = FITS_REAL8
      columns(i)%unit = 'rad'
      columns(i)%repcount = 1
   enddo
   columns(1)%name = 'theta'
   columns(2)%name = 'phi'
   columns(3)%name = 'psi'

   call fits_insert_bintab(outhandle,columns)

   nleft = nosamples
   time = starttime
   offset = 0
   do
      n = min(nleft,nbuffer)
      if (n==0) exit

      call detector_pointing_angles(theta,phi,psi,n,time,shift)

      call fits_write_column(outhandle,1,theta(1:n),offset)
      call fits_write_column(outhandle,2,phi(1:n),offset)
      call fits_write_column(outhandle,3,psi(1:n),offset)

      time = time+n/fsample
      nleft = nleft-n
      offset = offset+n
   enddo

   call fits_close(outhandle)
   write(*,*) 'Done'

END PROGRAM


