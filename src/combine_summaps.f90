PROGRAM combine_summaps
!
! Construct IQU maps from an arbitrary combination of sum maps.
! Elina Keih�nen 2012
! The code reuses parts of IQU_mapr

   use planck_config
   use fitsmod2

   implicit none

   ! Define parameters
     character(len=200),allocatable :: file_summap(:)
     character(len=200),allocatable :: file_matrix(:)
     character(len=200)   :: file_outmap = ''
     integer              :: nmaps = 0
     integer              :: nside = -1
     integer              :: nstokes = 3
     integer              :: ncc = 6
     real(dp),allocatable :: weight(:)
     character(len=200)   :: key, value, line, file_param

     integer             :: imap, iend, nopar, i, j, k, n, istep
     integer             :: nside_in, degrade, npix, npix_in
     integer(i8b)        :: offset
     logical             :: found, sing

     real(dp)             :: rcond_p, pixlim_map = 1.d-6
     real(dp),allocatable :: map(:,:), cc(:,:)
     real(dp),allocatable :: cc_p(:,:), map_p(:,:), buffer(:)
     real(dp),parameter   :: Healpix_undef = -1.6375e30

     type(fitshandle)   :: in_fits
     type(fitshandle)   :: out_fits
     type(fitscolumn)   :: columns(3) 

   ! Read input parameters
     nopar = command_argument_count()

     if (nopar==0) then 
        write(*,*) ''
        write(*,*) 'This tool constructs IQU maps from sum maps and pixel matrices.'
        write(*,*) '' 
        write(*,*) 'Usage ./combine_summaps params.par'
        write(*,*) ''
        write(*,*) 'Parameters:' 
        write(*,*) '   nside            = Resolution parameter. Default=resolution of the input maps'
        write(*,*) '   nstokes          = Number of Stokes components, 1 or 3. Default=3'
        write(*,*) '   nmaps            = Number of input maps.'
        write(*,*) '   file_summap_N    = Nth input sum map (FITS).'
        write(*,*) '   file_matrix_N    = Nth input matrix (FITS).'
        write(*,*) '   weight_N         = Relative weight for Nth map. Default=1.'
        write(*,*) '   file_outmap      = Output map name. (FITS file)'
        write(*,*) ''
     endif

     call get_command_argument(1, file_param)

     inquire(file=file_param,exist=found)
     if (.not.found) call exit_with_status(1,'Parameter file not found.')
     write(*,*) '' 

     nmaps = 0

     open(unit=12,file=trim(file_param))
     do
        read(12,'(a)',iostat=iend) line
        if (iend.lt.0) exit

        line = adjustl(line)
        if (line(1:1)=='#') cycle
        if (len_trim(line)==0) cycle

        n = index(line,'=')
        if (n.le.0) cycle

        key = line(1:n-1)
        value = adjustl(line(n+1:200))

        if (key=='nside') then 
           read(value,*) nside
        elseif (key=='file_outmap') then
           file_outmap = value
        elseif (key=='nstokes') then
           read(value,*) nstokes
        elseif (key=='nmaps') then 
           read(value,*) nmaps
           write(*,*) 'nmaps =',nmaps
           allocate(file_matrix(nmaps))
           allocate(file_summap(nmaps))
           allocate(weight(nmaps))
           file_matrix = ''
           file_summap = ''
           weight = 1.d0
        elseif (key(1:12)=='file_matrix_') then
           read(key(13:14),*) k
           if (k.gt.nmaps) call exit_with_status(1,'Too many matrix files.')
           file_matrix(k) = value
        elseif (key(1:12)=='file_summap_') then
           read(key(13:14),*) k
           if (k.gt.nmaps) call exit_with_status(1,'Too many map files.')
           file_summap(k) = value
        elseif (key(1:7)=='weight_') then
           read(key(8:9),*) k
           if (k.gt.nmaps) call exit_with_status(1,'Too many weight factors.')
           read(value,*) weight(k)
        else
           call exit_with_status(1, 'Unrecognized parameter '//trim(key))
        endif
     enddo

     do imap = 1,nmaps
        write(*,*) 'file_matrix = ', trim(file_matrix(imap))
        write(*,*) 'file_summap = ', trim(file_summap(imap))
        write(*,*) 'weight      = ', weight(imap)
     enddo
     write(*,*) 'nstokes         = ', nstokes
     write(*,*) 'file_outmap     = ', trim(file_outmap)

     if (nside.le.0) then
        call fits_open(in_fits,file_summap(1))
        call fits_get_key(in_fits,'nside',nside)
        call fits_close(in_fits)
     endif

     write(*,*) 'nside           = ', nside
     write(*,*)

     npix = nside*nside
     if (nstokes==1) ncc=1

   ! Create and initialize the output fits file
     call fits_create(out_fits, '!'//file_outmap)
     call fits_add_key(out_fits, 'CREATOR', 'combine_summaps')

     do k = 1,3
        columns(k)%repcount = npix
        columns(k)%type = fits_real4
     enddo

     columns(1)%name = 'I_Stokes'
     columns(2)%name = 'Q_Stokes'
     columns(3)%name = 'U_Stokes'

     call fits_insert_bintab(out_fits, columns(1:nstokes))

     call fits_add_comment(out_fits, '-----------------------------------')
     call fits_add_comment(out_fits, '   Healpix Map Specific Keywords   ')
     call fits_add_comment(out_fits, '-----------------------------------')

     call fits_add_key(out_fits, 'PIXTYPE', 'HEALPIX', 'HEALPIX Pixelization')
     call fits_add_key(out_fits, 'ORDERING', 'NESTED', 'Pixel ordering scheme')
     call fits_add_key(out_fits, 'NSIDE', nside, 'Resolution parameter for HEALPIX')
     call fits_add_key(out_fits, 'FIRSTPIX', 0, 'First pixel # (0 based)')
     call fits_add_key(out_fits, 'LASTPIX', 12*nside**2-1, 'Last pixel # (0 based)')
     call fits_add_key(out_fits, 'POLAR', (nstokes==3), 'Polarization included (True/False)')
     call fits_add_key(out_fits, 'POLCONV', 'COSMO', 'Coord. convention for polarization (COSMO/IAU)')

     allocate(map(npix,nstokes))
     map = 0.0
     allocate(cc(npix,ncc))
     cc = 0.0

!Work space for one pixel
     allocate(cc_p(nstokes,nstokes))
     cc_p = 0.0
     allocate(map_p(nstokes,1))
     map_p = 0.0

   ! Do the calculation in 12 pieces to save memory
     do istep = 1,12
        write(*,'(i6,a)') istep,' /12'
        map = 0.0
        cc = 0.0

        do imap = 1,nmaps
           call fits_open(in_fits,file_summap(imap))

           call fits_get_key(in_fits,'nside',nside_in)
           npix_in = nside_in*nside_in
           degrade = npix_in/npix

           if (degrade==0) then
               write(*,*) 'file =',trim(file_summap(imap))
               write(*,*) 'nside_in, nside_out =',nside_in,nside
               call exit_with_status(1,'Output resolution exceeds that of the input map.')
           endif

           allocate(buffer(npix_in))
           buffer = 0.0

           offset = (istep-1)*npix_in
           do k = 1,nstokes
              call fits_read_column(in_fits,k,buffer,offset)
              do i = 1,npix
                 do j = 1,degrade
                    map(i,k) = map(i,k) +weight(imap)*buffer((i-1)*degrade+j)
                 enddo
              enddo
           enddo
           call fits_close(in_fits)

           call fits_open(in_fits,file_matrix(imap))
           do k = 1,ncc
              call fits_read_column(in_fits,k,buffer,offset)
              do i = 1,npix
                 do j = 1,degrade
                    cc(i,k) = cc(i,k) +weight(imap)*buffer((i-1)*degrade+j)
                 enddo
              enddo
           enddo
           call fits_close(in_fits)

           deallocate(buffer)
        enddo

!Extract the relevant submatrix and solve
        do i = 1,npix
           if (nstokes==1) then
              cc_p(1,1) = cc(i,1)
           else
              cc_p(1,1) = cc(i,1)
              cc_p(2,1) = cc(i,2)
              cc_p(3,1) = cc(i,3)
              cc_p(1,2) = cc(i,2)
              cc_p(2,2) = cc(i,4)
              cc_p(3,2) = cc(i,5)
              cc_p(1,3) = cc(i,3)
              cc_p(2,3) = cc(i,5)
              cc_p(3,3) = cc(i,6)
           endif

           call invert_eig(cc_p,nstokes,rcond_p,sing) 

           if (rcond_p.lt.pixlim_map.or.sing) then
              map(i,:) = Healpix_undef
           else
              map_p(:,1) = map(i,:)
              map_p = matmul(cc_p, map_p)
              map(i,:)= map_p(:,1)
           endif
        enddo 

        offset = (istep-1)*npix
        do k = 1,nstokes
           call fits_write_column(out_fits,k,map(:,k),offset)  
        enddo

     enddo

     call fits_close(out_fits) 
     write(*,*) 'Done'

CONTAINS

!------------------------------------------------------------------------------

   SUBROUTINE invert_eig(aa,n,rcond,sing)
!
! Invert a symmetric positive-definite nxn matrix through eigenvalue analysis.
! Optional flag SING is set if matrix is singular.
! In the case of a singular matrix, only the non-singular subspace is inverted.
! From E. Keihanen (A-S added the calculation of rcond)

      double precision,intent(inout)        :: aa(n,n)
      double precision, intent(out)         :: rcond
      integer,         intent(in)           :: n
      logical,         intent(out),optional :: sing
      integer                               :: nrot, i, k
      double precision                      :: v(n,n), d(n), di, a(n,n)

      a = aa

      call Jacobi(aa,n,d,v,nrot)

      rcond = abs(minval(d)/maxval(d)) 

      if (minval(d).lt.0) then
          write(*,*) d
!          write(*,*)
!          do k = 1,n
!             write(*,*) a(k,:)
!          enddo
!          stop
      endif

      if (present(sing)) sing=.false.

      aa = 0.0
      do k = 1,n
         if (abs(d(k)).lt.1.e-30) then
            if (present(sing)) sing=.true.
         else
            di = 1/d(k)
            
            do i = 1,n
               aa(:,i) = aa(:,i) +di*v(i,k)*v(:,k)
            enddo
         endif
      enddo

   END SUBROUTINE

!------------------------------------------------------------------------------

   SUBROUTINE Jacobi(a,n,d,v,nrot)
 !
 ! Eigenvalues d and eigenvectors v of a matrix a.
 ! Jacobi algorithm from Numerical Recipes
 ! From E. Keihanen
 !
      integer,         intent(in)    :: n
      integer,         intent(out)   :: nrot
      double precision,intent(inout) :: a(n,n)
      double precision,intent(out)   :: d(n), v(n,n)
      integer                        :: i, j, ip, iq
      double precision               :: c, g, h, s, sm, t, tau, theta, tresh
      double precision               :: b(n), z(n)

      v = 0.0
      do ip = 1,n
         v(ip,ip) = 1.d0
         d(ip) = a(ip,ip)
      enddo
      b = d
      z = 0.0

      nrot = 0
      do i = 1,50

         sm = 0.0
         do ip = 1,n-1
            do iq = ip+1,n
               sm = sm+abs(a(ip,iq))
            enddo
         enddo
         if (sm.eq.0) return
         if (i.lt.4) then
            tresh = 0.2*sm/(n*n)
         else
            tresh = 0.0
         endif

         do ip = 1,n-1
            do iq = ip+1,n
               g = 100.*abs(a(ip,iq))
               if ((i.gt.4).and.(abs(d(ip))+g.eq.abs(d(ip))).and.  &
                               (abs(d(iq))+g.eq.abs(d(iq)))) then
                  a(ip,iq) = 0.
               elseif (abs(a(ip,iq)).gt.tresh) then
                  h = d(iq)-d(ip)
                  if (abs(h)+g.eq.abs(h)) then
                     t = a(ip,iq)/h
                  else
                     theta = 0.5d0*h/a(ip,iq)
                     t = 1/(abs(theta)+sqrt(1+theta*theta))
                     if (theta.lt.0) t=-t
                  endif
                  c = 1/sqrt(1+t*t)
                  s = t*c
                  tau = s/(1+c)
                  h = t*a(ip,iq)
                  z(ip) = z(ip)-h
                  z(iq) = z(iq)+h
                  d(ip) = d(ip)-h
                  d(iq) = d(iq)+h
                  a(ip,iq) = 0.
                  do j = 1,ip-1
                     g = a(j,ip)
                     h = a(j,iq)
                     a(j,ip) = g-s*(h+g*tau)
                     a(j,iq) = h+s*(g-h*tau)
                  enddo
                  do j = ip+1,iq-1
                     g = a(ip,j)
                     h = a(j,iq)
                     a(ip,j) = g-s*(h+g*tau)
                     a(j,iq) = h+s*(g-h*tau)
                  enddo
                  do j = iq+1,n
                     g = a(ip,j)
                     h = a(iq,j)
                     a(ip,j) = g-s*(h+g*tau)
                     a(iq,j) = h+s*(g-h*tau)
                  enddo
                  do j = 1,n
                     g = v(j,ip)
                     h = v(j,iq)
                     v(j,ip) = g-s*(h+g*tau)
                     v(j,iq) = h+s*(g-h*tau)
                  enddo
                  nrot = nrot+1
               endif
            enddo
         enddo
         b = b+z
         d = b
         z = 0.
      enddo
      write(*,*) 'Too many iterations'
      stop

   END SUBROUTINE

!-------------------------------------------------------------------------------

END PROGRAM
