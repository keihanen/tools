Program pixel_centers
!
! This program computes the centers on Healpix pixels in nested scheme.
! The coordinates are stored as a Healpix map.
! Columns: theta, phi
!
! Usage: ./pixel_centers nside
!
! Uses the Healpix routine pix2ang_nest

   use fitsmod2
   use planck_config

   implicit none
   integer           :: nside, ipix, nopix
   character(len=80) :: fileout, cpar, cnside
   type(fitshandle)  :: out
   type(fitscolumn)  :: cols(2)
   real,allocatable  :: theta(:),phi(:)
   real(dp)          :: dtheta,dphi

   integer, dimension(0:1023) :: pix2x=0, pix2y=0

   if (command_argument_count()==0) then
      write(*,*) 'This program stores the centers of Healpix pixels', &
                 ' in nested scheme.'
      write(*,*) ' The coordinates are stored as a Healpix map in file ', &
                 'pixel_centers_NSIDE.fits'
      write(*,*) ' Columns: theta, phi'
      write(*,*) 'Usage: ./pixel_centers nside'
      stop
   endif

   call get_command_argument(1,cpar)
   read(cpar,*) nside
   write(cnside,*) nside

   fileout = 'pixel_centers_'//trim(adjustl(cnside))//'.fits'

   nopix = 12*nside*nside
   allocate(theta(0:nopix-1),phi(0:nopix-1))

   do ipix = 0,nopix-1
      call pix2ang_nest(nside,ipix,dtheta,dphi)
      theta(ipix) = dtheta
      phi(ipix) = dphi
   enddo

   call fits_create(out,fileout)

   cols(1)%name = 'theta'
   cols(1)%unit = 'rad'
   cols(1)%repcount = nside*nside
   cols(1)%type = FITS_REAL4

   cols(2)%name = 'phi'
   cols(2)%unit = 'rad'
   cols(2)%repcount = nside*nside
   cols(2)%type = FITS_REAL4

   call fits_insert_bintab(out,cols)

   call fits_write_column(out,1,theta)
   call fits_write_column(out,2,phi)
   call fits_close(out)

CONTAINS

  subroutine pix2ang_nest(nside, ipix, theta, phi)
    !=======================================================================
    !     renders theta and phi coordinates of the nominal pixel center
    !     for the pixel number ipix (NESTED scheme)
    !     given the map resolution parameter nside
    !=======================================================================
    INTEGER, INTENT(IN) :: nside, ipix
    DOUBLE PRECISION, INTENT(OUT) :: theta, phi

    INTEGER :: npix, npface, &
         &     ipf, ip_low, ip_trunc, ip_med, ip_hi, &
         &     jrt, jr, nr, jpt, jp, kshift, nl4
    DOUBLE PRECISION :: z, fn, fact1, fact2

    INTEGER :: ix, iy, face_num
!     common /xy_nest/ ix, iy, face_num ! can be useful to calling routine

    ! coordinate of the lowest corner of each face
    INTEGER, dimension(1:12) :: jrll = (/ 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4 /) ! in unit of nside
    INTEGER, dimension(1:12) :: jpll = (/ 1, 3, 5, 7, 0, 2, 4, 6, 1, 3, 5, 7 /) ! in unit of nside/2
    !-----------------------------------------------------------------------
    npix = 12 * nside**2
 
    !     initiates the array for the pixel number -> (x,y) mapping
    if (pix2x(1023) <= 0) call mk_pix2xy()

    fn = nside
    fact1 = 1.d0/(3*fn*fn)
    fact2 = 2.d0/(3*fn)
    nl4   = 4*nside

    !     finds the face, and the number in the face
    npface = nside**2

    face_num = ipix/npface  ! face number in {0,11}
    ipf = MODULO(ipix,npface)  ! pixel number in the face {0,npface-1}

    !     finds the x,y on the face (starting from the lowest corner)
    !     from the pixel number
    ip_low = MODULO(ipf,1024)       ! content of the last 10 bits
    ip_trunc =   ipf/1024        ! truncation of the last 10 bits
    ip_med = MODULO(ip_trunc,1024)  ! content of the next 10 bits
    ip_hi  =     ip_trunc/1024   ! content of the high weight 10 bits

    ix = 1024*pix2x(ip_hi) + 32*pix2x(ip_med) + pix2x(ip_low)
    iy = 1024*pix2y(ip_hi) + 32*pix2y(ip_med) + pix2y(ip_low)

    !     transforms this in (horizontal, vertical) coordinates
    jrt = ix + iy  ! 'vertical' in {0,2*(nside-1)}
    jpt = ix - iy  ! 'horizontal' in {-nside+1,nside-1}

    !     computes the z coordinate on the sphere
    jr =  jrll(face_num+1)*nside - jrt - 1   ! ring number in {1,4*nside-1}

    nr = nside                  ! equatorial region (the most frequent)
    z  = (2*nside-jr)*fact2
    kshift = MODULO(jr - nside, 2)
    if (jr < nside) then     ! north pole region
       nr = jr
       z = 1.d0 - nr*nr*fact1
       kshift = 0
    else if (jr > 3*nside) then ! south pole region
       nr = nl4 - jr
       z = - 1.d0 + nr*nr*fact1
       kshift = 0
    endif
    theta = ACOS(z)

    !     computes the phi coordinate on the sphere, in [0,2Pi]
    jp = (jpll(face_num+1)*nr + jpt + 1 + kshift)/2  ! 'phi' number in the ring in {1,4*nr}
    if (jp > nl4) jp = jp - nl4
    if (jp < 1)   jp = jp + nl4

    phi = (jp - (kshift+1)*0.5d0) * (halfpi / nr)

    return
  end subroutine pix2ang_nest


  subroutine mk_pix2xy()
    !=======================================================================
    !     constructs the array giving x and y in the face from pixel number
    !     for the nested (quad-cube like) ordering of pixels
    !
    !     the bits corresponding to x and y are interleaved in the pixel number
    !     one breaks up the pixel number by even and odd bits
    !=======================================================================
    INTEGER ::  kpix, jpix, ix, iy, ip, id

    !cc cf block data      data      pix2x(1023) /0/
    !-----------------------------------------------------------------------
    !      print *, 'initiate pix2xy'
    do kpix=0,1023          ! pixel number
       jpix = kpix
       IX = 0
       IY = 0
       IP = 1               ! bit position (in x and y)
!        do while (jpix/=0) ! go through all the bits
       do
          if (jpix == 0) exit ! go through all the bits
          ID = MODULO(jpix,2)  ! bit value (in kpix), goes in ix
          jpix = jpix/2
          IX = ID*IP+IX

          ID = MODULO(jpix,2)  ! bit value (in kpix), goes in iy
          jpix = jpix/2
          IY = ID*IP+IY

          IP = 2*IP         ! next bit (in x and y)
       enddo
       pix2x(kpix) = IX     ! in 0,31
       pix2y(kpix) = IY     ! in 0,31
    enddo

    return
  end subroutine mk_pix2xy

End program
