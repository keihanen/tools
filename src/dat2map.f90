PROGRAM modifymap

! This tool reads an ascii table and writes it out as a Healpix temperature map in fits format.
! As default the maps is assumed to be in ring pixeling. This can be changed by key -nested.
!
! Usage: dat2ymap inmap.dat outmap.fits [-nested]

   use healpix_routines
   use fitsmod2
   use planck_config

   implicit none
   integer              :: nopar, ipar, nside, npix, i, k, iend
   integer(i8b)         :: off, chunksize, nout
   character(len=300)   :: file_in, file_out
   character(len=80)    :: ordering, ckey, cpar
   type(fitshandle)     :: out
   logical              :: nested
   type(fitscolumn)     :: column(1)
   real(sp),allocatable :: map(:)

   ordering="RING"
 
   nopar = command_argument_count()

   if (nopar.lt.2) then
      write(*,*) 'This tool transforms an ascii table into a Healpix map.'
       write(*,*) 'Usage: dat2map inmap.dat outmap.fits [-nested]'
      stop
   endif

   call get_command_argument(1,file_in)
   call get_command_argument(2,file_out)

   ipar = 2
   do 
      ipar = ipar+1
      if (ipar.gt.nopar) exit
      call get_command_argument(ipar,ckey)
      if (ckey=='-nested') then
         ordering="NESTED"
      else
         write(*,*) 'Unrecognized argument: ',trim(ckey)
         stop
      endif
   enddo

!Read input map
!First count rows
   open(unit=10,file=file_in)
   npix = 0
   do
      read(10,'(a)',iostat=iend) cpar
      if (iend.lt.0) exit
      if (len_trim(cpar).gt.0) npix=npix+1
   enddo
   write(*,*) 'npix =',npix

   nside=sqrt(npix/12+.1)

   rewind(10)
   allocate(map(npix))
   do i = 1,npix
      read(10,*) map(i)
   enddo
   close(10)
 
   call fits_create(out,'!'//trim(file_out))

   column(1)%type=FITS_REAL4
   column(1)%repcount = 1

   call fits_insert_bintab(out,column)

   call fits_add_key(out,'PIXTYPE', 'HEALPIX','HEALPIX Pixelisation')
   call fits_add_key(out,'ORDERING',trim(ordering), 'Pixel ordering scheme')
   call fits_add_key(out,'NSIDE',nside,'Resolution parameter for HEALPIX')
   call fits_add_key(out,'FIRSTPIX',0,'First pixel # (0 based)')
   call fits_add_key(out,'LASTPIX',npix-1,'Last pixel # (0 based)')
   call fits_add_key(out,'POLAR',.false.,'Polarization flag')

   chunksize = fits_fast_nelms(out,1)
   off = 0
   do while(off.lt.npix)
      nout = min(chunksize,npix-off)

      call fits_write_column(out,1,map(off+1:off+nout),off)
      off = off+nout
   enddo

   call fits_close(out)   

END PROGRAM
