program map_hist

! Display a histogram of values in a fitsio file.
! Usage: map_hist filename [-col N] [-bin X] [-log] [-lin] '
! The code divides the positive space into bins uniformly in logaritmic 
!   scale, and displays separately the number of negative and positive 
!   values within each bin.

   use planck_config
   use fitsmod2

   implicit none
   real(sp),parameter   :: Healpix_undef = -1.6375e30
   integer              :: nopar, ipar, nzeros, nundef, npos, nneg, nbin
   integer              :: i, k, k1, k2, n, icol, ncols, nsize
   real(dp)             :: mmin, mmax, absmin, absmax
   real(dp)             :: logmin, logmax, dbin, linmin, linmax
   character(len=200)   :: file_map
   character(len=20)    :: cpar, cvalue
   logical              :: found, logscale
   type(fitshandle)     :: handle
   integer, allocatable :: loghist(:,:), linhist(:)
   real(sp),allocatable :: llim(:), hlim(:)
   real(sp),allocatable :: map(:)

   nopar = command_argument_count()

   if (nopar==0) then
      write(*,*) 'Displays a histogram of a column of a fits file.'
      write(*,*) 'Usage: histmap filename [-col N] [-bin X] [-log/-lin] '
      write(*,*) '   -col N: column index  (default 1)'
      write(*,*) '     -log: logarithmic scale (default)'
      write(*,*) '     -lin: linear scale'
      write(*,*) '   -bin X: bin width.'
      write(*,*) ' If logarithmic scale is selected, code displays negative and positive values separately.'
      write(*,*) '   Bin is then the step in log_10 (default 1.0).'
      stop
   else
      call get_command_argument(1,file_map)
   endif
 
   dbin = -1.0d0
   icol = 1
   logscale = .true.

   ipar = 2
   do 
      if (ipar.gt.nopar) exit
      call get_command_argument(ipar,cpar)
      if (cpar=='-col') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) icol
      elseif (cpar=='-bin') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) dbin
      elseif (cpar=='-log') then
         logscale = .true. 
      elseif (cpar=='-lin') then
         logscale = .false. 
      else
         call exit_with_status(1, 'ERROR: Unrecognized argument '//trim(cpar))
      endif
      ipar = ipar+1
   enddo

   inquire(file=file_map,exist=found)
   if (.not.found) call exit_with_status(1, 'File not found: '//trim(file_map))

   call fits_open(handle,file_map)

   ncols = size(handle%columns)
   if (icol.gt.ncols) call exit_with_status(1,'ERROR: the file does not have enough columns.')

   nsize = handle%nrows*handle%columns(icol)%repcount
   allocate(map(nsize))

   write(*,*) 'Reading file...'
   call fits_read_column(handle,icol,map)
   call fits_close(handle)

   write(*,*) 'Ordering...'
   call rquicksort(map,nsize)

! Find bin limits
   nzeros = 0
   nundef = 0
   nneg = 0
   npos = 0

   found = .false.
   mmin = 0.0
   mmax = 0.0
   absmin = 0.0
   absmax = 0.0

   do i = 1,nsize
      if (map(i)==Healpix_undef) then
         nundef = nundef+1
         cycle
      endif

      if (map(i)==0) then
         nzeros = nzeros+1
      elseif (map(i).gt.0) then
         npos = npos+1
      else
         nneg = nneg+1
      endif

      if (.not.found) then
         mmin = map(i)
         mmax = map(i)
         found = .true.
      endif

      if (map(i).gt.mmax)  mmax=map(i)
      if (map(i).lt.mmin)  mmin=map(i)
      if (abs(map(i)).gt.0) then
         if (abs(map(i)).gt.absmax) absmax=abs(map(i))
         if (abs(map(i)).lt.absmin.or.absmin.le.0) absmin=abs(map(i))
      endif
   enddo

   write(*,'(i10,a)') nundef,' Healpix_undef'
   write(*,'(i10,a)') nzeros,' zeros'
   write(*,'(i10,a)') npos,  ' positive values'
   write(*,'(i10,a)') nneg,  ' negative values'
   write(*,'(i10,a)') nsize, ' total' 
   write(*,'(a,2es12.3)') ' Min,max =',mmin, mmax

   if (npos==0.and.nneg==0) call exit_with_status(0)
   if (logscale) then

      write(*,*) 'Computing logarithmic histogram...'
      if (dbin.lt.0) dbin=1.d0
      write(*,'(a,f8.5)') ' dbin =',dbin

! Find bin limits

      if (absmin.le.0.or.absmax.le.0) call exit_with_status(0)
      logmin = floor(log10(absmin))
      logmax = ceiling(log10(absmax))
      if (logmin.lt.-30) logmin=-30.
      if (logmax.le.logmin) call exit_with_status(0)
 
      nbin = ceiling((logmax-logmin)/dbin)
 
      allocate(llim(nbin),hlim(nbin))
      allocate(loghist(nbin,2))
 
      do k = 1,nbin
         llim(k)= 10.**(logmin+(k-1)*dbin)
         hlim(k)= 10.**(logmin+k*dbin)
      enddo
 
 ! Compute the histogram
      loghist = 0
      k1 = 1
      k2 = nbin
      do i = 1, nsize
         if (map(i)==Healpix_undef) then
             cycle
         elseif (map(i).gt.0) then
            do while(map(i).ge.hlim(k1).and.k1.lt.nbin) 
               k1 = k1+1
            enddo
            loghist(k1,1) = loghist(k1,1)+1
         elseif (map(i).lt.0) then
            do while(abs(map(i)).lt.llim(k2).and.k2.gt.1) 
              k2 = k2-1
            enddo
            loghist(k2,2) = loghist(k2,2)+1
         endif
      enddo

      write(*,*) '    lim_low    lim_high       pos       neg'

      do k = 1, nbin
         write(*,'(2es12.3,2i10)')   &
           llim(k),hlim(k),loghist(k,1),loghist(k,2)
      enddo
      write(*,*)
      deallocate(map,loghist,llim,hlim)

   else

      write(*,*) 'Computing linear histogram...'

! Find bin limits
      if (dbin.lt.0) then
         dbin = (mmax-mmin)/10
         dbin = floor(log10(dbin))
         dbin = 10**dbin
      endif
      write(*,'(a,f8.5)') ' dbin =',dbin

      k1 = mmin/dbin
      k2 = mmax/dbin+1
      nbin = k2-k1+1
 
      allocate(llim(nbin),hlim(nbin))
      allocate(linhist(nbin))
 
      do k = 1,nbin
        llim(k)= (k1+k-1)*dbin
        hlim(k)= (k1+k)*dbin
      enddo
 
 ! Compute the histogram
      linhist = 0
      k = 1
      do i = 1, nsize
         if (map(i).ne.Healpix_undef) then
            do while(map(i).ge.hlim(k).and.k.lt.nbin) 
               k = k+1
            enddo
            linhist(k) = linhist(k)+1
         endif
      enddo

      write(*,*) '    lim_low    lim_high       n'

      do k = 1, nbin
         write(*,'(2es12.3,i10)')   &
           llim(k),hlim(k),linhist(k)
      enddo
      write(*,*)
      deallocate(map,linhist,llim,hlim)

   endif
 
 CONTAINS
   
 SUBROUTINE rquicksort(array, n)
 ! Sorting by quick_sort algorithm without recursion using stack array.

    implicit none
    real,    intent(inout)  :: array(n)
    integer, intent(in)     :: n
    integer, parameter      :: M=7, NSTACK=52
    integer                 :: stack(NSTACK)
    integer                 :: i, j, left, right, middle, stack_length
    real                    :: a, temp

    stack_length = 0
    left = 1
    right = n
    do
       if (right-left < M) then
          ! insertion sort when only few numbers
          do j = left+1, right
             a = array(j)
             i = j
             do while (i>left .AND. array(i-1)>a)
                array(i) = array(i-1)
                i = i-1
             enddo
             array(i) = a
          enddo
          if (stack_length==0) exit
          right = stack(stack_length)
          left = stack(stack_length-1)
          stack_length = stack_length-2  

       else
          middle = (left+right)/2
          temp = array(middle)
          array(middle) = array(left+1)
          array(left+1) = temp
          if (array(left)>array(right)) then
             temp = array(left)
             array(left) = array(right)
             array(right) = temp
          endif
          if (array(left+1)>array(right)) then
             temp = array(left+1)
             array(left+1) = array(right)
             array(right) = temp
          endif
          if (array(left)>array(left+1)) then
             temp = array(left)
             array(left) = array(left+1)
             array(left+1) = temp
          endif
          i = left+1
          j = right
          a = array(left+1)

          do
             i = i+1
             do while (array(i).lt.a)
                i = i+1
             enddo
             j = j-1
             do while (array(j).gt.a)
                j = j-1
             enddo
             if (j < i) exit
             temp = array(i)
             array(i) = array(j)
             array(j) = temp
          enddo

          array(left+1) = array(j)
          array(j) = a
          stack_length = stack_length+2
          if (stack_length > NSTACK) then
             write (*,*) 'Error in quick_sort:  NSTACK is too small'
             stop
          endif
          if (right-i+1.ge.j-left) then
             stack(stack_length) = right
             stack(stack_length-1) = i
             right = j-1
          else
             stack(stack_length) = j-1
             stack(stack_length-1) = left
             left = i
          endif
       endif
   enddo

 END SUBROUTINE rquicksort

end program
