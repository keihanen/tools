PROGRAM findmean 

! Find the mean of a Healpix map.
! Usage: findmean map.fits [frac]
! The fraction FRAC of pixels with highest values are masked out.
! This is useful when one wants to know the monopole of the CMB component
!   in a map which contains foreggrounds. Default is frac=0.1.

   use planck_config
   use fitsmod2

   integer              :: nopar, ipar
   integer              :: nside, nopix, imap, nmax, n, i
   character(len=100)   :: filename, cpar
   real(sp),allocatable :: map(:)
   integer, allocatable :: index(:)
   real(dp)             :: frac=0.1d0
   real(dp)             :: dsum
   type(fitshandle)     :: handle

   nopar = command_argument_count()

   if (nopar==1) then
      call get_command_argument(1,filename)
   elseif (nopar==2) then
      call get_command_argument(1,filename)
      call get_command_argument(2,cpar)
      read(cpar,*) frac
   else
      write(*,*) 'Usage: ./findmean map.fits [frac (default 0.1)]'
   endif

   call fits_open(handle,filename)
   call fits_get_key(handle,'NSIDE',nside)
   nopix = 12*nside*nside

   allocate(map(nopix),index(nopix))

   do imap = 1,size(handle%columns)

      call fits_read_column(handle,imap,map)

      n = 0
      do i = 1,nopix
         if (map(i).gt.-1e30) then
            n = n+1
            map(n) = map(i)
         endif
      enddo
      nmax = (1.d0-frac)*n

      call quicksort(map,index,n)

      dsum = 0
      do i = 1,nmax
         dsum = dsum+map(i)
      enddo
      dsum = dsum/nmax

      write(*,'(i6,f12.6,es16.6)') imap,dsum,dsum

   enddo

   call fits_close(handle)
   deallocate(map,index)

!------------------------------------

CONTAINS

   SUBROUTINE quicksort(array,index,n)
 ! Sorting by quick_sort algorithm without recursion using stack array.

      implicit none
      real, intent(inout)    :: array(n)
      integer, intent(out)   :: index(n)
      integer, intent(in)    :: n
      integer, parameter     :: M=7, NSTACK=52
      integer                :: stack(NSTACK)
      integer                :: i, j, left, right, middle, stack_length
      integer                :: ia, itemp
      real                   :: a, temp

      do i = 1, n
         index(i) = i
      enddo
      stack_length = 0
      left = 1
      right = n
      do
         if (right-left < M) then
          ! insertion sort when only few numbers
            do j = left+1, right
               ia = index(j)
               a = array(j)
               i = j
               do while (i>left .AND. array(i-1)>a)
                  index(i) = index(i-1)
                  array(i) = array(i-1)
                  i = i-1
               enddo
               index(i) = ia
               array(i) = a
            enddo
            if (stack_length==0) exit
            right = stack(stack_length)
            left = stack(stack_length-1)
            stack_length = stack_length-2

         else
            middle = (left+right)/2
            itemp = index(middle)
            temp = array(middle)
            index(middle) = index(left+1)
            array(middle) = array(left+1)
            index(left+1) = itemp
            array(left+1) = temp
            if (array(left)>array(right)) then
               itemp = index(left)
               temp = array(left)
               index(left) = index(right)
               array(left) = array(right)
               index(right) = itemp
               array(right) = temp
            endif
            if (array(left+1)>array(right)) then
               itemp = index(left+1)
               temp = array(left+1)
               index(left+1) = index(right)
               array(left+1) = array(right)
               index(right) = itemp
               array(right) = temp
            endif
            if (array(left)>array(left+1)) then
               itemp = index(left)
               temp = array(left)
               index(left) = index(left+1)
               array(left) = array(left+1)
               index(left+1) = itemp
               array(left+1) = temp
            endif
            i = left+1
            j = right
            ia = index(left+1)
            a = array(left+1)

            do
               i = i+1
               do while (array(i).lt.a)
                  i = i+1
               enddo
               j = j-1
               do while (array(j).gt.a)
                  j = j-1
               enddo
               if (j < i) exit
               itemp = index(i)
               temp = array(i)
               index(i) = index(j)
               array(i) = array(j)
               index(j) = itemp
               array(j) = temp
            enddo

            index(left+1) = index(j)
            array(left+1) = array(j)
            index(j) = ia
            array(j) = a
            stack_length = stack_length+2
            if (stack_length > NSTACK) then
               write (*,*) 'Error in quick_sort:  NSTACK is too small'
               stop
            endif
            if (right-i+1.ge.j-left) then
               stack(stack_length) = right
               stack(stack_length-1) = i
               right = j-1
            else
               stack(stack_length) = j-1
               stack(stack_length-1) = left
               left = i
            endif
         endif
      enddo

   END SUBROUTINE quicksort

!-----------------------------------------------------------------------


END PROGRAM
