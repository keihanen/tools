PROGRAM factorize

   implicit none
   integer :: n, k
   character(len=20) :: cn=''

   call getarg(1,cn)

   read(cn,*) n

   write(*,*) 'n =',n
   write(*,*) 'Factors:'

   k = 2
   do
      if (mod(n,k)==0) then
         write(*,*) k
         n = n/k
      else
         k = k+1
         if (k.gt.n) exit
      endif
   enddo

END PROGRAM
