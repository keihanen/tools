PROGRAM mapprofile

! This tool computes the "profile" of a Healpix map,
! i.e. integrated temperature as a function of latitude.
! Usage: mapprofile map.fits profile.dat

   use healpix_routines
   use fitsmod2
   use planck_config

   implicit none
   integer              :: nopar, ipar, nside, nmaps
   integer              :: i, imap, npix, np, irow, ipmax
   character(len=200)   :: file_in, file_out
   character(len=10)    :: ordering
   character(len=20)    :: ckey, cvalue
   type(fitshandle)     :: in
   type(fitscolumn),allocatable :: columns(:)
   real(dp),allocatable :: map(:), profile(:,:), area(:)

   nopar = command_argument_count()

   if (nopar.lt.2) then
      write(*,*) 'This tool computes the "profile" of a Healpix map, ' // &
                 'i.e. integrated temperature as a function of latitude.'
      write(*,*) 'Usage:  mapprofile map.fits profile.dat'
      call exit_with_status(1)
   endif

   call get_command_argument(1,file_in)
   call get_command_argument(2,file_out)

   call fits_open(in,file_in)

   call fits_get_key(in,'nside',nside)
   call fits_get_key(in,'ordering',ordering)
   nmaps = size(in%columns)

   write(*,*)
   write(*,*)          'Input map: ',trim(file_in)
   write(*,'(x,a,i8)') 'nside    = ',nside
   write(*,*)          'ordering = '//trim(ordering)

   npix = 12*nside*nside
   allocate(map(npix))
   map = 0.0

   allocate(profile(4*nside,nmaps))
   profile = 0.0
   allocate(area(4*nside))
   area = 0.0

   do imap = 1,nmaps
      write(*,'(" imap =",i4," /",i4)') imap,nmaps
      call fits_read_column(in,imap,map)

      if (ordering=='nested'.or.ordering=="NESTED") then
         write(*,*) 'Transforming to ring pixeling'
         call nestedmap_to_ring(map)
      endif

      irow = 0
      np = 0
      ipmax = 0
      do i=1,npix

         if (i.gt.ipmax) then
            irow = irow+1
            if (irow<=nside) then
               np = np+4
            elseif (irow>3*nside) then
               np = np-4
            endif
            ipmax = ipmax+np

            area(irow+1) = area(irow)+np 
            profile(irow+1,imap) = profile(irow,imap)
         endif

         if (map(i).gt.Healpix_undef) &
             profile(irow+1,imap) = profile(irow+1,imap) +map(i)
      enddo
   enddo

   call fits_close(in)   
   deallocate(map)

   open(unit=20,file=file_out)
   do irow = 1,4*nside
      write(20,'(f10.6,2x,10es16.7)') 1-2*area(irow)/npix, (profile(irow,imap)/npix,imap=1,nmaps)
   enddo
   close(20)
   write(*,*) 'Profile written in file ',trim(file_out) 

   deallocate(profile,area)

END PROGRAM
