program map_hist

! Display histogram of a integer-valued fitsio file.
! Usage: hit_hist filename [-col n]

   use planck_config
   use fitsmod2

   implicit none
   integer              :: nopar, ipar, i, k, n, icol, ncols, nsize
   character(len=20)    :: cpar, cvalue
   character(len=200)   :: file_map
   logical              :: found
   type(fitshandle)     :: handle
   integer,allocatable  :: map(:)

   nopar = command_argument_count()

   if (nopar==0) then
      write(*,*) 'Displays a histogram of the contents of integer-valued fits file.'
      write(*,*) 'Usage: hit_hist filename [-col N]'
      write(*,*) '  -col N: column index  (default 1)'
      stop
   else
      call get_command_argument(1,file_map)
   endif

   icol = 1

   ipar = 2
   do 
      if (ipar.gt.nopar) exit
      call get_command_argument(ipar,cpar)
      if (cpar=='-col') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) icol
      else
         call exit_with_status(1, 'ERROR: Unrecognized argument '//trim(cpar))
      endif
      ipar = ipar+1
   enddo

   inquire(file=file_map,exist=found)
   if (.not.found) call exit_with_status(1, 'File not found: '//trim(file_map))

   call fits_open(handle,file_map)

   ncols = size(handle%columns)
   if (icol.gt.ncols) call exit_with_status(1,'ERROR: the file does not have enough columns.')

   nsize = handle%nrows*handle%columns(icol)%repcount
   allocate(map(nsize))

   write(*,*) 'Reading file...'
   call fits_read_column(handle,icol,map)
   call fits_close(handle)

   write(*,*) 'Ordering...'
   call rquicksort(map,nsize)

   write(*,*) '      value       count'
   n = 1
   k = map(1)
   do i = 2,nsize
      if (map(i)==k) then
         n = n+1
      else
         write(*,'(2i12)') k,n
         k = map(i)
         n = 1
      endif
   enddo

   write(*,'(2i12)') k,n
   write(*,'(a,i15)') '   Total ',sum(int(map,i8b))
   write(*,*)

   deallocate(map)

 CONTAINS
   
 SUBROUTINE rquicksort(array, n)
 ! Sorting by quick_sort algorithm without recursion using stack array.

    implicit none
    integer, intent(inout)  :: array(n)
    integer, intent(in)     :: n
    integer, parameter      :: M=7, NSTACK=52
    integer                 :: stack(NSTACK)
    integer                 :: i, j, left, right, middle, stack_length
    integer                 :: a, temp

    stack_length = 0
    left = 1
    right = n
    do
       if (right-left < M) then
          ! insertion sort when only few numbers
          do j = left+1, right
             a = array(j)
             i = j
             do while (i>left .AND. array(i-1)>a)
                array(i) = array(i-1)
                i = i-1
             enddo
             array(i) = a
          enddo
          if (stack_length==0) exit
          right = stack(stack_length)
          left = stack(stack_length-1)
          stack_length = stack_length-2  

       else
          middle = (left+right)/2
          temp = array(middle)
          array(middle) = array(left+1)
          array(left+1) = temp
          if (array(left)>array(right)) then
             temp = array(left)
             array(left) = array(right)
             array(right) = temp
          endif
          if (array(left+1)>array(right)) then
             temp = array(left+1)
             array(left+1) = array(right)
             array(right) = temp
          endif
          if (array(left)>array(left+1)) then
             temp = array(left)
             array(left) = array(left+1)
             array(left+1) = temp
          endif
          i = left+1
          j = right
          a = array(left+1)

          do
             i = i+1
             do while (array(i).lt.a)
                i = i+1
             enddo
             j = j-1
             do while (array(j).gt.a)
                j = j-1
             enddo
             if (j < i) exit
             temp = array(i)
             array(i) = array(j)
             array(j) = temp
          enddo

          array(left+1) = array(j)
          array(j) = a
          stack_length = stack_length+2
          if (stack_length > NSTACK) then
             write (*,*) 'Error in quick_sort:  NSTACK is too small'
             stop
          endif
          if (right-i+1.ge.j-left) then
             stack(stack_length) = right
             stack(stack_length-1) = i
             right = j-1
          else
             stack(stack_length) = j-1
             stack(stack_length-1) = left
             left = i
          endif
       endif
   enddo

 END SUBROUTINE rquicksort

end program
