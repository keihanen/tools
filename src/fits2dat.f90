PROGRAM fits2dat
!Transform a fits file into an ascii file

   use fitsmod2
   use planck_config

   implicit none
   integer            :: nopar, ipar, i, k, n, icol, ncols, nrows
   integer            :: dtype, itab, imax, nf
   character(len=200) :: file_in, file_out
   type(fitshandle)   :: handle
   integer,parameter  :: ndig = 9
   character(len=24)  :: tform
   character(len=90)  :: str
   logical            :: str_present

   type(fitscolumn),  allocatable :: columns(:)
   integer,           allocatable :: icolumn(:)
   real(dp),          allocatable :: dcolumn(:)
   character(len=80), allocatable :: strcolumn(:)
   character(len=400),allocatable :: table(:)

   nopar = command_argument_count()

   if (nopar==0) then
      write(*,*) 'This tool reads a fits file and writes its contents into an ascii file.'
      write(*,*) 'Output file name is the same as the input file name, with ".dat" appended.'
      write(*,*) 'Usage: fits2dat file.fits'
      call exit_with_status(1)
   endif

   call get_command_argument(1,file_in)

   k = index(file_in,'.',.true.)
   if (k.le.0) k=len_trim(file_in)+1

   if (file_in(k:k+3)==".dat") call exit_with_status(1,"Looks like an ascii file already")

   file_out=file_in(1:k-1) // ".dat"
   call fits_open(handle,trim(file_in),fits_readonly,2)
   open(unit=10,file=file_out)

   ncols = size(handle%columns)
   nrows = 0
   str_present = .false.
   do icol = 1,ncols
      if (handle%columns(icol)%type==FITS_CHAR) then
         n = handle%nrows
         str_present = .true.
      else
         n = handle%nrows*handle%columns(icol)%repcount
      endif
      nrows = max(nrows,n)
   enddo

   allocate(dcolumn(nrows))
   allocate(icolumn(nrows))
   if (str_present) allocate(strcolumn(nrows))
   allocate(table(nrows))
   table = ''

   itab = 0
   do icol = 1,ncols

      if (dtype==FITS_CHAR) then
         n = handle%nrows
      else
         n = handle%nrows*handle%columns(icol)%repcount
      endif

      dtype = handle%columns(icol)%type

      if (dtype==fits_int8.or.dtype==fits_int4) then

         call fits_read_column(handle,icol,icolumn(1:n))

         imax = maxval(abs(icolumn(1:n)))

         if (imax.lt.10000) then
            tform = '(i6)'
            nf = 6
         elseif (imax.lt.1e7) then
            tform = '(i9)'
            nf = 9
         else
            tform = '(i13)'
            nf = 13
         endif

         do i = 1,n
            write(str,tform) icolumn(i)
            table(i)(itab+1:itab+nf) = str(1:nf)
         enddo
         itab = itab+nf

      elseif (dtype==fits_real4.or.dtype==fits_real8) then

         call fits_read_column(handle,icol,dcolumn(1:n))

         nf = ndig+9
         write(tform,'("(g",i0,".",i0,")")') nf,ndig

         do i = 1,n
            write(str,tform) dcolumn(i)
            table(i)(itab+1:itab+nf) = str(1:nf)
         enddo
         itab = itab+nf

      elseif (dtype==fits_char) then

         call fits_read_column(handle,icol,strcolumn(1:n))

         nf = handle%columns(icol)%repcount+2
         write(tform,'("(2x,a",i0,")")') nf-2
         do i = 1,n
            write(str,tform) strcolumn(i)(1:nf-2)
            table(i)(itab+1:itab+nf) = str(1:nf)
         enddo
         itab = itab+nf

      endif

  enddo

  do i = 1,n
     write(10,'(a)') trim(table(i))
  enddo

  call fits_close(handle)
  close(10)

  deallocate(table,icolumn,dcolumn)
  if (str_present) deallocate(strcolumn)

END PROGRAM
