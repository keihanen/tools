PROGRAM combinemaps

! Linear combination of fitsio files

   use planck_config
   use fitsmod2

   implicit none
   real(sp),parameter :: Healpix_undef = -1.6375e30
   integer            :: nopar, nofiles, ifile, ncols, icol, nside, nside_out, i, ikey
   character(len=200) :: filenames(20), file_out
   character(len=20)  :: cpar, ordering, ordering_out
   real(dp)           :: weights(20), dkey
   type(fitshandle),target      :: infiles(20), out
   type(fitshandle),pointer     :: in
   type(fitscolumn),allocatable :: columns(:)
   integer(i8b),    allocatable :: collengths(:)

   integer(i8b)             :: nleft, nbuff_max, nbuff, n, offset
   integer(i8b),allocatable :: i8buffer(:), i8outbuffer(:)
   real(dp),    allocatable :: dbuffer(:), doutbuffer(:)
   logical                  :: keyfound

   nside_out = -1
   ordering_out = ''

   nopar = command_argument_count()

   if (nopar==0) then
      write(*,*) 'Linear combination of fitsio files'
      write(*,*) 'Syntax: ./combinemaps file1.fits weight1 file2.fits ', &
                                        'weight2... file_out.fits'
      write(*,*) 'Files must be of equal size, shape, and type.'
      stop
   endif

   if (mod(nopar,2)==0.or.nopar==1) then
      write(*,*) 'Bad number of arguments'
      stop
   endif

   nofiles = nopar/2
   do ifile = 1, nofiles
      call get_command_argument(2*ifile-1,filenames(ifile))
      call get_command_argument(2*ifile,cpar)
      read(cpar,*) weights(ifile)
   enddo
   call get_command_argument(nopar,file_out)

!Open the first file and get file size/type etc from there
   in => infiles(1)
   call fits_open(in,filenames(1))

   ncols = size(in%columns)

   allocate(columns(ncols))
   allocate(collengths(ncols))

   do icol = 1,ncols
      columns(icol)%name = in%columns(icol)%name
      columns(icol)%unit = in%columns(icol)%unit
      columns(icol)%type = in%columns(icol)%type
      columns(icol)%repcount = in%columns(icol)%repcount

      collengths(icol) = in%nrows*in%columns(icol)%repcount
   enddo

   keyfound = fits_key_present(in,'nside')
   if (keyfound) call fits_get_key(in,'nside',nside_out)

   keyfound = fits_key_present(in,'ordering')
   if (keyfound) call fits_get_key(in,'ordering',ordering_out)

!Open the rest of the files and check that types match
   do ifile = 2,nofiles
      in => infiles(ifile)
      call fits_open(in,filenames(ifile))

      if (size(in%columns).ne.ncols) then
         write(*,*) 'ERROR: File sizes do not match.'
         stop
      endif

      keyfound = fits_key_present(in,'nside')
      if (keyfound) then
         call fits_get_key(in,'nside',nside)
         if (nside.ne.nside_out) nside_out=-1
      endif

      keyfound = fits_key_present(in,'ordering')
      if (keyfound) then
         call fits_get_key(in,'ordering',ordering)
         if (ordering.ne.ordering_out)then
            write(*,*) 'WARNING: Keyword "ordering" does not match.'
            ordering_out = ''
         endif
      endif

      do icol = 1,ncols
         if (in%columns(icol)%type.ne.columns(icol)%type) then
            write(*,*) 'ERROR: Column types do not match.'
            stop
         endif

         if (in%nrows*in%columns(icol)%repcount.ne.collengths(icol)) then
            write(*,*) 'ERROR: Column sizes do not match.'
            stop
         endif

         if (in%columns(icol)%name.ne.columns(icol)%name) then
            write(*,*) 'Warning: Column names do not match.'
            columns(icol)%name = ''
         endif

         if (in%columns(icol)%unit.ne.columns(icol)%unit) then
            write(*,*) 'Warning: column units do not match.'
            columns(icol)%unit = ''
         endif

         if (in%columns(icol)%repcount.ne.columns(icol)%repcount) then
            write(*,*) 'Warning: Repcounts do not match.'
            columns(icol)%repcount = 1
         endif
      enddo

   enddo

   call fits_create(out,"!"//trim(file_out))
   call fits_insert_bintab(out,columns)

!If a Healpix map, write keywords
   if (nside_out.gt.0.and.len_trim(ordering_out).gt.0) then
      call fits_add_key(out,'PIXTYPE', 'HEALPIX','HEALPIX Pixelisation')
      call fits_add_key(out,'ORDERING',ordering, 'Pixel ordering scheme')
      call fits_add_key(out,'NSIDE',nside_out,'Resolution parameter for HEALPIX')
      call fits_add_key(out,'FIRSTPIX',0,'First pixel # (0 based)')
      call fits_add_key(out,'LASTPIX',12*nside_out**2-1,'Last pixel # (0 based)')
      call fits_add_key(out,'POLAR',(ncols==3),'Polarization flag')
   else

!Copy some other potentially relevant keywords
   if (fits_key_present(in,'NSIDE')) then
      call fits_get_key(in,'NSIDE',ikey)
      call fits_add_key(out,'NSIDE', ikey)
   endif
   if (fits_key_present(in,'NPSI')) then
      call fits_get_key(in,'NPSI',ikey)
      call fits_add_key(out,'NPSI', ikey)
   endif
   if (fits_key_present(in,'ORDERING')) then
      call fits_get_key(in,'ORDERING',ordering)
      call fits_add_key(out,'ORDERING', ordering)
   endif
   if (fits_key_present(in,'PSIPOL')) then
      call fits_get_key(in,'PSIPOL',dkey)
      call fits_add_key(out,'PSIPOL', dkey)
   endif
   if (fits_key_present(in,'SIGMA')) then
      call fits_get_key(in,'SIGMA',dkey)
      call fits_add_key(out,'SIGMA', dkey)
   endif
   endif

! Read data and combine
   nbuff_max = 12*1024*1024

   do icol = 1,ncols
      nbuff = min(collengths(icol),nbuff_max)

      if (columns(icol)%type==fits_int8.or.   &
              columns(icol)%type==fits_int4) then
         allocate(i8buffer(nbuff))
         allocate(i8outbuffer(nbuff))

         nleft = collengths(icol)
         offset = 0
         do
            if (nleft==0) exit
            n = min(nleft,nbuff)

            i8outbuffer = 0 
            do ifile = 1,nofiles
               call fits_read_column(infiles(ifile),icol,i8buffer(1:n),offset)
               i8outbuffer(1:n) = i8outbuffer(1:n) +weights(ifile)*i8buffer(1:n)
            enddo
            call fits_write_column(out,icol,i8outbuffer(1:n),offset)
            nleft = nleft-n
            offset = offset+n
         enddo
         deallocate(i8buffer,i8outbuffer)

      elseif (columns(icol)%type==fits_real8.or.   &
              columns(icol)%type==fits_real4) then
         allocate(dbuffer(nbuff))
         allocate(doutbuffer(nbuff))

         nleft = collengths(icol)
         offset = 0
         do
            if (nleft==0) exit
            n = min(nleft,nbuff)

            doutbuffer = 0 
            do ifile = 1,nofiles
               call fits_read_column(infiles(ifile),icol,dbuffer(1:n),offset)
               do i = 1,n
                  if (dbuffer(i)==Healpix_undef) then
                      doutbuffer(i)=Healpix_undef
                  else
                     doutbuffer(i) = doutbuffer(i) +weights(ifile)*dbuffer(i)
                  endif
               enddo
            enddo
            call fits_write_column(out,icol,doutbuffer(1:n),offset)
            nleft = nleft-n
            offset = offset+n
         enddo
         deallocate(dbuffer,doutbuffer)
      endif
   enddo

   do ifile = 1,nofiles
      call fits_close(infiles(ifile))
   enddo
   call fits_close(out)
   deallocate(columns,collengths)

END PROGRAM
