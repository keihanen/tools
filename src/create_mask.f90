PROGRAM modifymap

! This tool creates a sky mask
!   and performs up/downgrading to given resolution.
!
   use healpix_routines
   use fitsmod2
   use planck_config

   implicit none
   integer              :: nopar, ipar
   integer              :: nside_in, nside_out, nopix_in, nopix_out
   integer              :: i, k, m, n, mode, icol
   integer(i8b)         :: offset, chunksize, nout
   real(dp)             :: treshold, skycover
   logical              :: reverse, real_output
   character(len=100)   :: file_in, file_out
   character(len=10)    :: ordering_in, ordering_out, ordering_wrk
   character(len=20)    :: cpar
   type(fitshandle)     :: in, out
   type(fitscolumn)     :: column(1)
   real(sp),allocatable :: map(:)
   integer, allocatable :: mask(:)

   nside_out = -1
   ordering_out = ''
   treshold = 0.0
   mode = 1
   reverse = .false.
   real_output = .false.
   icol = 1

   nopar = command_argument_count()

   if (nopar.lt.2) then
      write(*,*) 'CREATE_MASK'
      write(*,*) 'Given as input a temperature map, this tool builds a sky mask.'
      write(*,*) 'Pixels with temperature above the treshold are ', &
                 'marked by value 1, others by 0.'
      write(*,*) 'Optionally the code also changes the pixeling scheme between ring/nested'
      write(*,*) ' and up/downgrades the mask to given resolution.'
      write(*,*) 'The code can also be used to up/downgrade of an existing mask.'
      write(*,*)
      write(*,*) 'Usage: ./create_mask inmap.fits out.fits [-key value]'
      write(*,*) 
      write(*,*) 'inmap.fits: Input map. Can have integer or real values.'
      write(*,*) 'out.fits:   Output mask.'
      write(*,*) 'Options:'
      write(*,*) '-col:       Column read. Default=1.'
      write(*,*) '-tr:        Treshold value for pixels to be included in the output mask.'
      write(*,*) '            Values greater than this are accepted (equal values are not). Default=0.0.'
      write(*,*) '-nside:     Output resolution. Default=input resolution'  
      write(*,*) '-ordering:  Ordering of the output map, ring/nested. Default=ordering of the input map.'
      write(*,*) '-mode:      Downgrading mode (only relevant if the mask is downgraded)'
      write(*,*) '              1 = only accept pixels with all subpixels above the treshold (default)'
      write(*,*) '              2 = accept pixels with any subpixel above the treshold'
      write(*,*) '-rev:       (T/F) Reverse the mask, i.e. accept only pixels below or ', &
                              'equal to the treshold. Default=F.'
      write(*,*) '-real:      (T/F) Produce a real-valued mask. Default=F (integer).'
      stop
   endif

   call get_command_argument(1,file_in)
   call get_command_argument(2,file_out)

   do ipar = 3,nopar,2
      call get_command_argument(ipar,cpar)

      if (cpar=='-nside') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) nside_out
      elseif (cpar(1:9)=='-ordering') then
         call get_command_argument(ipar+1,cpar)
         ordering_out = trim(cpar)
      elseif (cpar(1:3)=='-tr') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) treshold
      elseif (cpar(1:4)=='-rev') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) reverse
      elseif (cpar(1:5)=='-real') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) real_output
      elseif (cpar(1:4)=='-col') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) icol
      elseif (cpar=='-mode') then
         call get_command_argument(ipar+1,cpar)
         read(cpar,*) mode
         if (mode.ne.1.and.mode.ne.2) then
            write(*,*) 'ERROR in arguments: mode can take values 1 or 2'
            stop
         endif
      else
         write(*,*) 'Unrecognized parameter: ',trim(cpar)
         stop
      endif
   enddo

   call fits_open(in,file_in)

   call fits_get_key(in,'nside',nside_in)
   call fits_get_key(in,'ordering',ordering_in)
   if (ordering_in=='RING')   ordering_in='ring'
   if (ordering_in=='NESTED') ordering_in='nested'

   write(*,*)
   write(*,*)          'Input file: ',trim(file_in)
   write(*,'(x,a,i8)') 'nside    = ',nside_in
   write(*,*)          'ordering = '//trim(ordering_in)
   write(*,'(x,a,i8)') 'column   = ',icol

   if (ordering_out=='RING')   ordering_out='ring'
   if (ordering_out=='NESTED') ordering_out='nested'
 
   if (nside_out.lt.0) nside_out=nside_in
   if (len_trim(ordering_out)==0) ordering_out=ordering_in

   write(*,*)          'Output file: ',trim(file_out)
   write(*,'(x,a,i8)') 'nside    = ',nside_out
   write(*,*)          'ordering = '//trim(ordering_out)

   write(*,'(x,a,es12.3)') 'Treshold    ',treshold
   if (reverse) write(*,*) 'Reverse masking'
   if (real_output) then
      write(*,*) 'Real-valued mask'
   else
      write(*,*) 'Integer mask'
   endif

   if (nside_out.lt.nside_in) write(*,'(x,a,i4)') 'Downgrading mode',mode

   nopix_in = 12*nside_in*nside_in
   nopix_out = 12*nside_out*nside_out

   allocate(map(0:nopix_in-1))
   map = 0.0

   call fits_read_column(in,icol,map)
   call fits_close(in)

   if (ordering_in=='ring'.and.ordering_out=='nested') then
      write(*,*) 'Transforming to nested pixeling'
      call ringmap_to_nested(map)
      ordering_wrk = 'nested'
   elseif (ordering_in=='ring'.and.nside_in.ne.nside_out) then
      write(*,*) 'Transforming to nested pixeling for up/downgrading'
      call ringmap_to_nested(map)
      ordering_wrk = 'nested'
   else
      ordering_wrk = ordering_in
   endif

! Create mask and up/downgrade

   allocate(mask(0:nopix_out-1))
   mask = 0.0

   if (nside_out==nside_in) then
      do i = 0,nopix_in-1
         if (map(i).gt.treshold) mask(i)=1
      enddo

   elseif (nside_out.gt.nside_in) then !upgrade
      n = nside_out**2/nside_in**2
      m = 0
      do i = 0,nopix_in-1
         if (map(i).gt.treshold) mask(m:m+n-1)=1
         m = m+n
      enddo

   elseif (nside_out.lt.nside_in) then !downgrade
      n = nside_in**2/nside_out**2
      if (mode==1) then
         m = 0
         do i = 0,nopix_out-1
            mask(i) = 1
            do k = 1,n
               if (map(m).le.treshold) mask(i)=0
               m = m+1
            enddo
         enddo
      elseif (mode==2) then
         m = 0
         do i = 0,nopix_out-1
            mask(i) = 0
            do k = 1,n
               if (map(m).gt.treshold) mask(i)=1
               m = m+1
            enddo
         enddo
      endif
   endif

   if (reverse) then
      do i = 0,nopix_out-1
         mask(i) = 1-mask(i)
      enddo
   endif

   if (ordering_wrk=='nested'.and.ordering_out=='ring') then
      write(*,*) 'Transforming the mask to ring pixeling.'

      call nestedmap_to_ring(mask)
   endif

   call fits_create(out,'!'//trim(file_out))

   column(1)%name = 'I_Stokes'
   column(1)%repcount = 1
   if (real_output) then
      column(1)%type = fits_real4
   else
      column(1)%type = fits_int4
   endif

   call fits_insert_bintab(out,column)

   call fits_add_key(out,'PIXTYPE', 'HEALPIX','HEALPIX Pixelisation')
   call fits_add_key(out,'ORDERING',trim(ordering_out), 'Pixel ordering scheme')
   call fits_add_key(out,'NSIDE',nside_out,'Resolution parameter for HEALPIX')
   call fits_add_key(out,'FIRSTPIX',0,'First pixel # (0 based)')
   call fits_add_key(out,'LASTPIX',nopix_out-1,'Last pixel # (0 based)')

   n = count(mask.gt.0)
   skycover = n/real(nopix_out)
   write(*,'(x,a,f10.5)') 'Sky coverage',skycover

   call fits_add_key(out,'GOODPIX',n,'Accepted pixels')
   call fits_add_key(out,'BADPIX',nopix_out-n,'Rejected pixels')
   call fits_add_key(out,'SKYCOV',real(skycover),'Sky coverage')

   chunksize = fits_fast_nelms(out,1)
   offset = 0
   do while(offset.lt.nopix_out)
      nout = min(chunksize,nopix_out-offset)
      call fits_write_column(out,1,mask(offset:offset+nout-1),offset)
      offset = offset+nout
   enddo

   call fits_close(out)   

   deallocate(mask)

END PROGRAM
