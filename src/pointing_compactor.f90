PROGRAM pointing_compactor
! Rewrite detector pointing in compact form.
! Pointing is rotated to galactic coordinates and converted to
! nested pixel number at resolution nside=4096.

   use planck_config
   use fitsmod2
   use coordinate_conversion
   use healpix_routines

   implicit none
   integer              :: ipar, nopar
   character(len=300)   :: infile, outfile
   integer, parameter   :: nside=4096, nbuff=100000
   real(dp),parameter   :: dipsi=4*twopi/1024/1024/1024
   integer(i8b)         :: offset, nleft, nsamples, chunksize, off, nout
   integer              :: i, n, ncols, repc
   real(dp),allocatable :: dtheta(:), dphi(:), dpsi(:)
   integer, allocatable :: ipix(:), ipsi(:)
   type(fitshandle)     :: in, out
   type(fitscolumn)     :: columns(2)

   nopar = command_argument_count()

   if (nopar.ne.2) then
      write(*,*) 'Rewrite detector pointing in compact form'
      write(*,*) 'Syntax: ./pointing_compactor file_in file_out'
      stop
   endif

   call get_command_argument(1,infile)
   call get_command_argument(2,outfile)

   call fits_open(in,trim(infile),fits_readonly)

   ncols = size(in%columns)
   if (ncols.ne.3) call exit_with_status(1,"Bad input file format: "//trim(infile))

   repc = in%columns(1)%repcount
   nsamples = in%nrows*repc

   columns(1)%repcount = repc
   columns(1)%type = FITS_INT4
   columns(1)%name = "pixel"
   columns(2)%repcount = repc
   columns(2)%type = FITS_INT4
   columns(2)%name = "ipsi"

   call fits_create(out,"!"//trim(outfile))
   call fits_insert_bintab(out,columns)
   chunksize = fits_fast_nelms(out,1)

   call fits_add_key(out,'NSIDE',nside,'Healpix resolution')
   call fits_add_key(out,'DIPSI',dipsi,'psi resolution')
   call fits_add_key(out,'ORDERING','NESTED','Pixel ordering scheme')
   call fits_add_key(out,'COORDSYS','GALACTIC','Coordinate system')

   allocate(dtheta(nbuff))
   allocate(dphi(nbuff))
   allocate(dpsi(nbuff))
   dtheta = 0.0
   dphi = 0.0
   dpsi = 0.0

   allocate(ipix(nbuff))
   allocate(ipsi(nbuff))
   ipix = 0
   ipsi = 0

   nleft = nsamples
   offset = 0

   do
! Read n samples at a time
       n = nbuff
       if (nleft.lt.n) n=nleft
       call fits_read_column(in,1,dtheta(1:n),offset)
       call fits_read_column(in,2,dphi(1:n),offset)
       call fits_read_column(in,3,dpsi(1:n),offset)

!Rotate to galactic coordinates and convert into pixel number
       call convert_angle(dtheta,dphi,dpsi,n,7)
       call angle_to_nested(ipix,dtheta,dphi,n,nside)

       do i = 1,n
          ipsi(i) = dpsi(i)/dipsi+.5
       enddo

       off = 0
       do while (off.lt.n)
          nout = min(chunksize,n-off)
          call fits_write_column(out,1,ipix(off+1:off+nout),offset+off)
          call fits_write_column(out,2,ipsi(off+1:off+nout),offset+off)
          off = off+nout
       enddo

       offset = offset+n
       nleft = nleft-n

       if (nleft==0) exit
    enddo

    call fits_close(in)
    call fits_close(out)

END PROGRAM
