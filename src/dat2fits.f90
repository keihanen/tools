PROGRAM dat2fits
!Transform a fits file into an ascii file

   use fitsmod2
   use planck_config

   implicit none
   integer            :: nopar, ipar, i, k, n, icol, ncols, nrows, iend, dtype
   character(len=200) :: file_in, file_out
   type(fitshandle)   :: handle
   character(len=40)  :: cpar, typestring
   logical            :: str_present, binary
   type(fitscolumn), allocatable :: columns(:)
   integer,           allocatable :: icolumn(:)
   real(dp),          allocatable :: dcolumn(:)
   character(len=40), allocatable :: strcolumn(:)

   nopar = command_argument_count()

   if (nopar==0.or.nopar.gt.3) then
      write(*,*) 'This tool reads an ascii file and writes its contents into a fits file.'
      write(*,*) 'Output file name is the same as the input file name, with ".fits" appended.'
      write(*,*) 'Usage: dat2fits file.dat [XX -bin]'
      write(*,*) 'XX is a string which defines the column data types, for example JDE.'
      write(*,*) 'Default is D (one double precision column.'
      write(*,*) 'If -bin is given, data is written into a binary table instead os ascii.'
      call exit_with_status(1)
   endif

   typestring = 'D'
   binary = .false.
   str_present = .false.

   call get_command_argument(1,file_in)
   do ipar = 2,nopar
      call get_command_argument(ipar,cpar)
      if (cpar=="-bin") then
         binary = .true.
      else
         typestring = cpar
      endif
   enddo

   ncols = len_trim(typestring)
   allocate(columns(ncols))

   do i = 1,ncols
      select case (typestring(i:i))
        case ('E','e')
          columns(i)%type=FITS_REAL4
        case ('D','d')
          columns(i)%type=FITS_REAL8
        case ('J','j')
          columns(i)%type=FITS_INT4
        case ('K','k')
          columns(i)%type=FITS_INT8
        case ('A','a')
          columns(i)%type=FITS_CHAR
          columns(i)%repcount = 40
          str_present = .true.
        case default
          call exit_with_status (1, 'FITS: wrong data type')
      end select
   enddo         

   k = index(file_in,'.',.true.)
   if (k.le.0) k=len_trim(file_in)+1

   if (file_in(k:k+4)==".fits") call exit_with_status(1,"Looks like a fits file already")
   file_out = file_in(1:k-1) // ".fits"

   open(unit=10,file=file_in)
   nrows = 0
   do
      read(10,'(a)',iostat=iend) cpar
      if (iend.lt.0) exit
      if (len_trim(cpar).gt.0) nrows=nrows+1
   enddo
   write(*,*) 'nrows =',nrows

   call fits_create(handle,trim(file_out))
   if (binary) then
      call fits_insert_bintab(handle,columns)
   else
      call fits_insert_asctab(handle,columns)
   endif

   allocate(dcolumn(nrows))
   allocate(icolumn(nrows))
   if (str_present) allocate(strcolumn(nrows))

   do icol = 1,ncols
      rewind(10)
      dtype = handle%columns(icol)%type

      if (dtype==fits_int8.or.dtype==fits_int4) then

         do i = 1,nrows
            read(10,*) (cpar,k=1,icol-1),icolumn(i)
         enddo
         call fits_write_column(handle,icol,icolumn)

      elseif (dtype==fits_real4.or.dtype==fits_real8) then

         do i = 1,nrows
            read(10,*)  (cpar,k=1,icol-1),dcolumn(i)
         enddo
         call fits_write_column(handle,icol,dcolumn)

      elseif (dtype==fits_char) then

         do i = 1,nrows
            read(10,*)  (cpar,k=1,icol-1),strcolumn(i)
         enddo
         call fits_write_column(handle,icol,strcolumn)

      endif
  enddo

  call fits_close(handle)
  close(10)

  deallocate(icolumn,dcolumn)
  if (str_present) deallocate(strcolumn)

END PROGRAM
