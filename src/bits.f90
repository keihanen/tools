PROGRAM bits


implicit none 
integer,parameter :: idp = selected_int_kind(18)
integer(idp) :: n
integer :: k
character(len=72) :: cn='', st=''

call getarg(1,cn)
read(cn,*) n

if (n.lt.256) then
   do k = 0,7
      if (btest(n,k)) then
         st = '1' // trim(st)
      else
         st = '0' // trim(st)
      endif
   enddo
   write(*,*) st(1:8)
elseif (n/2.le.2147483647) then
   do k = 0,31
      if (btest(n,k)) then
         st = '1' // trim(st)
      else
         st = '0' // trim(st)
      endif

      if (k==7.or.k==15.or.k==23) st = ' '//trim(st)
   enddo
   write(*,*) st(1:35)
else
   do k = 0,63
      if (btest(n,k)) then
         st = '1' // trim(st)
      else
         st = '0' // trim(st)
      endif

      if (mod(k+1,8)==0) st = ' '//trim(st)
   enddo
   write(*,*) st
endif
END
