program fshow

! FSHOW
! fshow displays the contents of a binary fitsio file
!
! Syntax: 
! fshow filename -options
!
   use planck_types
   use planck_config
   use fitsmod2

   implicit none
   integer            :: norows = -1
   integer            :: nhdu   =  2
   integer            :: norows_file, nocols_file
   logical            :: kcolumn(99) = .false.

   integer            :: ipar, nopar
   integer            :: i, k, itab, nf, icol, icol1, icol2, ntail
   integer            :: nbuffer, nleft, offset, n, nn, ndig, dtype, nr
   integer(i8b)       :: ioff, imax
   character(len=300) :: sarg, filename = ''
   character(len=24)  :: sno, tform
   character(len=90)  :: str
   type(fitshandle)   :: handle
   logical            :: write_linenumbers, write_header

   integer(i8b), allocatable :: icolumn(:)
   real(dp),     allocatable :: dcolumn(:)
   character(len=80),allocatable :: strcolumn(:)
   character(len=400),allocatable :: table(:)

   filename = ''
   norows  = -1
   nhdu    = 2
   offset  = 0
   ntail = -1
   ndig    = 5
   write_linenumbers = .false.
   write_header      = .true.

   nopar = command_argument_count()
   if (nopar==0) then
      write(*,*) 'Usage: fshow filename [options]'
      write(*,*) 'Options:'
      write(*,*) ' -n      n       Number of rows shown (default: show all)'
      write(*,*) ' -offset offset  Rows omitted (default=0)'
      write(*,*) ' -tail   n       Display n lines from the end of the file'
      write(*,*) ' Give either -n (and -offset) or -tail'
      write(*,*) ' -col    col     Column index (default: show all). First index 1.'
      write(*,*) '                 You can give a single column index (for example -col 1)'
      write(*,*) '                 or specify a range (for example -col 1-3).'
      write(*,*) ' -hdu    nhdu    HDU (default: 2)'
      write(*,*) ' -dig    n       Number of digits displayed (float)'
      write(*,*) ' -lines          Display line numbers'
      write(*,*) ' -dat            Do not show header information'

      stop
   endif

   ipar = 0
   do
      ipar = ipar+1
      if (ipar.gt.nopar) exit

      call get_command_argument(ipar,sarg)

      if (ipar==1) then
         filename = sarg

      elseif (sarg=='-hdu') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)
         read(sno,*) nhdu

      elseif (sarg(1:4)=='-off') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)
         read(sno,*) offset

      elseif (sarg=='-n') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)
         read (sno,*) norows

      elseif (sarg=='-tail') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)
         read (sno,*) ntail

      elseif (sarg=='-dig') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)
         read (sno,*) ndig

      elseif (sarg=='-col') then
         ipar = ipar+1
         call get_command_argument(ipar,sno)

         k = index(sno,'-')
         if (k.le.0) then  !column index given
            read (sno,*) icol
            kcolumn(icol) = .true.
         else  !A Range of column indeces given
            read(sno(1:k-1),*) icol1
            read(sno(k+1:20),*) icol2
            do icol = icol1,icol2
               kcolumn(icol) = .true.
            enddo
         endif

      elseif (sarg(1:4)=='-lin') then
         write_linenumbers = .true.

      elseif (sarg=='-dat') then
         write_header = .false.

      else
         write(*,*) 'Unrecognized argument: ',trim(sarg)
         stop
      endif
   enddo

!No column specified: display all
   if (all(.not.kcolumn)) kcolumn= .true.

   if (norows.gt.0.and.ntail.gt.0) then
      write(*,*) 'Error in arguments: Do not give both -n and -ntail'
      stop
   endif

   call fits_open(handle,trim(filename),fits_readonly,nhdu)

   nocols_file = size(handle%columns)
   norows_file = 0

   do icol = 1,nocols_file
      if (kcolumn(icol)) then
         if (handle%columns(icol)%type==FITS_CHAR) then
            nr = handle%nrows
         else
            nr = handle%nrows*handle%columns(icol)%repcount
         endif
         norows_file = max(norows_file,nr)
      endif
   enddo

   if (ntail.gt.0) then
      norows = ntail
      offset = norows_file-ntail
   endif

   if (offset.ge.norows_file) then
      write(*,*) 'Error: offset too large.'
      write(*,*) 'offset, nrows =',offset,norows_file
      stop
   endif

   if (write_header) then
      write(*,'(a)') 'Column       type          nrows        name'

      do icol = 1,nocols_file
         if (kcolumn(icol)) then
            dtype = handle%columns(icol)%type
            if (dtype==FITS_CHAR) then
               nr = handle%nrows
            else
               nr = handle%nrows*handle%columns(icol)%repcount
            endif
 
            write(*,'(i6,4x,a,i12,4x,a)') icol,     &
                type2string(fitstype2type(dtype)),  &
                nr, trim(handle%columns(icol)%name)
         endif
      enddo
      write(*,*)
   endif

   if (norows.lt.0) then
      norows = norows_file-offset
   else
      norows = min(norows,norows_file-offset)
   endif

   nbuffer = min(norows,1000)

   allocate(dcolumn(nbuffer))
   allocate(icolumn(nbuffer))
   allocate(strcolumn(nbuffer))
   allocate(table(nbuffer))

!Transform the data into a string and store in array TABLE.

   nleft = norows
   ioff = offset
   do
      if (nleft==0) exit

      nn = min(nbuffer,nleft)
      table = ''

      if (write_linenumbers) then

         if (offset+norows.lt.100) then
            tform = '(i2)'
            nf = 2
         elseif (offset+norows.lt.1e5) then
            tform = '(i5)'
            nf = 5
         elseif (offset+norows.lt.1e8) then
            tform = '(i8)'
            nf = 8
         else
            tform = '(i10)'
            nf = 10
         endif

         do i = 1,nn
            write(str,tform) ioff+i
            table(i)(1:nf) = str(1:nf)
         enddo
         itab = nf
      else
         itab = 0
      endif

      do icol = 1,nocols_file
         if (.not.kcolumn(icol)) cycle

         if (dtype==FITS_CHAR) then
            nr = handle%nrows
         else
            nr = handle%nrows*handle%columns(icol)%repcount
         endif

         n = min(nn,nr-ioff)
         dtype = handle%columns(icol)%type

         if (dtype==fits_int8.or.dtype==fits_int4.or.dtype==fits_int2.or.dtype==fits_int1) then

            call fits_read_column(handle,icol,icolumn(1:n),ioff)

            imax = maxval(abs(icolumn(1:n)))

            if (imax.lt.10000) then
               tform = '(i6)'
               nf = 6
            elseif (imax.lt.1e7) then
               tform = '(i9)'
               nf = 9
            elseif (imax.lt.1e11) then
               tform = '(i13)'
               nf = 13
            elseif (imax.lt.1e15) then
               tform = '(i17)'
               nf = 17
            else
               tform = '(i22)'
               nf = 22
            endif

            do i = 1,n
               write(str,tform) icolumn(i)
               table(i)(itab+1:itab+nf) = str(1:nf)
            enddo
            itab = itab+nf

         elseif (dtype==fits_real4.or.dtype==fits_real8) then

            call fits_read_column(handle,icol,dcolumn(1:n),ioff)

            nf = ndig+9
            write(tform,'("(g",i0,".",i0,")")') nf,ndig

            do i = 1,n
               write(str,tform) dcolumn(i)
               table(i)(itab+1:itab+nf) = str(1:nf)
            enddo
            itab = itab+nf

         elseif (dtype==fits_char) then

            call fits_read_column(handle,icol,strcolumn(1:n),ioff)

            nf = handle%columns(icol)%repcount+2
            write(tform,'("(2x,a",i0,")")') nf-2
            do i = 1,n
               write(str,tform) strcolumn(i)(1:nf-2)
               table(i)(itab+1:itab+nf) = str(1:nf)
            enddo
            itab = itab+nf

         endif

     enddo

     do i = 1,nn
        write(*,'(a)') trim(table(i))
     enddo

     ioff = ioff+nn
     nleft = nleft-nn
  enddo

  if (norows.gt.0) write(*,*)

  call fits_close(handle)
  deallocate(table,icolumn,dcolumn)

end program
