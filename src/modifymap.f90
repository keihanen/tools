PROGRAM modifymap

! This tool changes the pixeling of a Healpix map from ring to nested or vice versa,
!   and performs up/downgrading to given resolution.
!
! Usage: modifymap inmap.fits outmap.fits [-nside N -ordering SCHEME -fill_undef -rm_monopole]

   use healpix_routines
   use fitsmod2
   use planck_config
   use matrix

   implicit none
   integer              :: nopar, ipar
   integer              :: nside_in, nside_out, nside_mask, nside_cc, nmaps, ncols
   integer              :: i, k, m, n, ibase, imap, npix_in, npix_out
   integer(i8b)         :: offset, off, chunksize, nout
   real(dp)             :: mono, doff, cctmp(6), tmp(6), cdet
   character(len=300)   :: file_in, file_out, file_mask, file_cc
   character(len=10)    :: ordering_in, ordering_out, ordering_wrk, ordering_mask, &
                           ordering_cc
   character(len=20)    :: ckey, cvalue
   character(len=1)     :: dtype
   type(fitshandle)     :: in, out, inm, incc
   logical              :: undef, fill_undef, rm_monopole, sing
   type(fitscolumn),allocatable :: columns(:)
   real(sp),allocatable :: map(:,:), outbuffer(:,:), mask(:)
   real(dp),allocatable :: cc_in(:,:), cc_out(:,:)

   nside_out = -1
   nside_mask = -1
   ordering_out = ''
   ordering_mask = ''
   fill_undef = .false.
   rm_monopole = .false.
   file_mask = ''
   file_cc = ''
   dtype = ''
   doff = -1e30
   ncols = 0

   nopar = command_argument_count()

   if (nopar.lt.2) then
      write(*,*) 'This tool transforms a map from ring to nested pixeling scheme', &
                  'and up/downgrades the map to resolution nside.'
      write(*,*) 'Also allows to fill undefined pixels with zeros,'
      write(*,*) ' apply a mask, or remove the monopole of the map.'
      write(*,*) 'If a cc file is provided, downgrading is done through noise weighting,', &
                 'otherwise by naive averaging.'
      write(*,*) 'Usage: modifymap inmap.fits outmap.fits ', &
                 '[-nside N -ordering ring/nested -mask maskfile.fits -cc ccfile.fits ', &
                 '-fill_undef -rm_monopole -rm_offset x -add_offset x -ncols n -dtype r]'
      stop
   endif

   call get_command_argument(1,file_in)
   call get_command_argument(2,file_out)

   ipar = 2
   do 
      ipar = ipar+1
      if (ipar.gt.nopar) exit
      call get_command_argument(ipar,ckey)
      if (ckey=='-nside') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) nside_out
      elseif (ckey=='-ordering') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         ordering_out = cvalue 
      elseif (ckey=='-mask') then
         ipar = ipar+1
         call get_command_argument(ipar,file_mask)
      elseif (ckey=='-cc') then
         ipar = ipar+1
         call get_command_argument(ipar,file_cc)
      elseif (ckey=='-rm_offset') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) doff
      elseif (ckey=='-add_offset') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) doff
         doff = -doff
      elseif (ckey=='-ncols') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) ncols
      elseif (ckey=='-fill_undef') then
         fill_undef = .true.
      elseif (ckey=='-rm_monopole') then
         rm_monopole = .true.
      elseif (ckey=='-dtype') then
         ipar = ipar+1
         call get_command_argument(ipar,cvalue)
         read(cvalue,*) dtype
      else
         write(*,*) 'Unrecognized argument: ',trim(ckey)
         stop
      endif
   enddo

!Read input map
   call fits_open(in,file_in)

   call fits_get_key(in,'nside',nside_in)
   call fits_get_key(in,'ordering',ordering_in)
   nmaps = size(in%columns)
   if (ncols>0) nmaps=ncols

   if (nside_out.lt.0) nside_out=nside_in
   if (ordering_out=='') ordering_out=ordering_in

   write(*,*)
   write(*,*)          'Input map: ',trim(file_in)
   write(*,'(x,a,i8)') 'nside    = ',nside_in
   write(*,*)          'ordering = '//trim(ordering_in)
   write(*,'(x,a,i8)') 'nmaps   = ',nmaps
   write(*,*)          'Output map: ',trim(file_out)
   write(*,'(x,a,i8)') 'nside    = ',nside_out
   write(*,*)          'ordering = '//trim(ordering_out)

   if (ordering_in=='RING')   ordering_in='ring'
   if (ordering_in=='NESTED') ordering_in='nested'
   if (ordering_out=='RING')   ordering_out='ring'
   if (ordering_out=='NESTED') ordering_out='nested'

   npix_in = 12*nside_in*nside_in
   allocate(map(npix_in,nmaps))
   map = 0.0

   if (ordering_in=='ring'.and.ordering_out=='nested') then
      write(*,*) 'Transforming to nested pixeling'
      ordering_wrk = 'nested'
   elseif (ordering_in=='ring'.and.nside_in.ne.nside_out) then
      write(*,*) 'Transforming to nested pixeling for up/downgrading'
      ordering_wrk = 'nested'
   else
      ordering_wrk = ordering_in
   endif

   do imap = 1,nmaps
      call fits_read_column(in,imap,map(:,imap))
   enddo

   if (ordering_in=='ring'.and.ordering_wrk=='nested') &
      call ringmap_to_nested(map)

!Read mask
   if (len_trim(file_mask).gt.0) then
      call fits_open(inm,file_mask)
      call fits_get_key(inm,'nside',nside_mask)
      call fits_get_key(inm,'ordering',ordering_mask)
      if (ordering_mask=='RING')   ordering_mask='ring'
      if (ordering_mask=='NESTED') ordering_mask='nested'

      write(*,*)          'Mask:      ',trim(file_mask)
      write(*,'(x,a,i8)') 'nside    = ',nside_mask
      write(*,*)          'ordering = '//trim(ordering_mask)

      if (nside_mask.ne.nside_in) then
         write(*,*) 'ERROR: Mask resolution must equal the input map resolution.'
         write(*,*) 'Use create_mask to down/upgrade the mask.'
         call exit_with_status(1)
      endif

      allocate(mask(npix_in))
      call fits_read_column(inm,1,mask)
      call fits_close(inm)

      if (ordering_mask=='ring'.and.ordering_wrk=='nested') then
         call ringmap_to_nested(mask)
      elseif (ordering_mask=='nested'.and.ordering_wrk=='ring') then
         call nestedmap_to_ring(mask)
      endif
   endif

!Read cc
   if (len_trim(file_cc).gt.0) then
      if (nside_out.gt.nside_in) then
         write(*,*) 'ERROR: Noise weighting not possible when upgrading.'
         call exit_with_status(1)
      endif
      if (nmaps.ne.3) then
         write(*,*) 'ERROR: Noise weighting only works for polarization maps.'
         call exit_with_status(1)
      endif

      call fits_open(incc,file_cc)
      call fits_get_key(incc,'nside',nside_cc)
      call fits_get_key(incc,'ordering',ordering_cc)
      if (ordering_cc=='RING')   ordering_cc='ring'
      if (ordering_cc=='NESTED') ordering_cc='nested'

      write(*,*)          'CC:      ',trim(file_cc)
      write(*,'(x,a,i8)') 'nside    = ',nside_cc
      write(*,*)          'ordering = '//trim(ordering_cc)

      if (nside_cc.ne.nside_in) then
         write(*,*) 'ERROR: CC resolution must equal the input map resolution.'
         call exit_with_status(1)
      endif

      allocate(cc_in(npix_in,6))
      do imap = 1,6
         call fits_read_column(incc,imap,cc_in(:,imap))
      enddo
      call fits_close(incc)

      if (ordering_cc=='ring'.and.ordering_wrk=='nested') then
         call ringmap_to_nested(cc_in)
      elseif (ordering_cc=='nested'.and.ordering_wrk=='ring') then
         call nestedmap_to_ring(cc_in)
      endif
   endif

   npix_out = 12*nside_out*nside_out
   allocate(outbuffer(npix_out,nmaps))
   outbuffer = 0.0

   if (allocated(mask)) then
      do i = 1,npix_in
         if (mask(i).le.0) map(i,:)=Healpix_undef
      enddo
   endif

! up/downgrade
   if (nside_out==nside_in) then
      do i = 1,npix_in
         outbuffer(i,:) = map(i,:)
      enddo

   elseif (nside_out.gt.nside_in) then
      n = nside_out**2/nside_in**2
      m = 0
      do i = 1,npix_in
         do k = 1,n
            m = m+1
            outbuffer(m,:) = map(i,:)
         enddo
      enddo

   elseif (nside_out.lt.nside_in.and..not.allocated(cc_in)) then
!Simple downgrading
      n = nside_in**2/nside_out**2
      do imap = 1,nmaps
         do i = 1,npix_out
            outbuffer(i,imap) = 0.0
            undef = .false.
            m = (i-1)*n
            do k = 1,n
               m = m+1
               outbuffer(i,imap) = outbuffer(i,imap) +map(m,imap)/n

               if (map(m,imap)==Healpix_undef) undef=.true.
            enddo
            if (undef) outbuffer(i,imap)=Healpix_undef
         enddo
      enddo

   elseif (nside_out.lt.nside_in.and.allocated(cc_in)) then
!Noise-weighted downgrading
      allocate(cc_out(npix_out,6))
      cc_out = 0.0

      do i = 1,npix_in
         if (any(map(i,:)==Healpix_undef)) then
             map(i,:) = Healpix_undef
         else
            tmp = map(i,:)
            cctmp = cc_in(i,:)
            map(i,1) = cctmp(1)*tmp(1)+cctmp(2)*tmp(2)+cctmp(3)*tmp(3)
            map(i,2) = cctmp(2)*tmp(1)+cctmp(4)*tmp(2)+cctmp(5)*tmp(3)
            map(i,3) = cctmp(3)*tmp(1)+cctmp(5)*tmp(2)+cctmp(6)*tmp(3)
         endif
      enddo

      n = nside_in**2/nside_out**2
      do i = 1,npix_out
         undef = .false.
         m = (i-1)*n
         do k = 1,n
            m = m+1
            outbuffer(i,:) = outbuffer(i,:) +map(m,:)
            cc_out(i,:) = cc_out(i,:) +cc_in(m,:)

            if (map(m,1)==Healpix_undef) undef=.true.
         enddo
         if (undef) outbuffer(i,:)=Healpix_undef
      enddo

      do i = 1,npix_out
         if (outbuffer(i,1).eq.Healpix_undef) cycle

         cctmp = cc_out(i,:)
         call invert3_LU(cctmp,cdet,sing)
         if (sing) then
            outbuffer(i,:) = Healpix_undef
            cycle
         endif          

         tmp = outbuffer(i,:)
         outbuffer(i,1) = cctmp(1)*tmp(1)+cctmp(2)*tmp(2)+cctmp(3)*tmp(3)
         outbuffer(i,2) = cctmp(2)*tmp(1)+cctmp(4)*tmp(2)+cctmp(5)*tmp(3)
         outbuffer(i,3) = cctmp(3)*tmp(1)+cctmp(5)*tmp(2)+cctmp(6)*tmp(3)
      enddo

   endif

   if (ordering_out=='ring'.and.ordering_wrk=='nested') &
      call nestedmap_to_ring(outbuffer)

   if (rm_monopole) then
      n = 0
      mono = 0.0
      do i = 1,npix_out
         if (outbuffer(i,1).gt.Healpix_undef) then
            mono = mono+outbuffer(i,1)
            n = n+1
         endif
      enddo
      if (n.gt.0) mono=mono/n

      do i = 1,npix_out
         if (outbuffer(i,1).gt.Healpix_undef) outbuffer(i,1)=outbuffer(i,1)-mono
      enddo
      write(*,'(x,a,g16.7)') 'monopole =',mono
   endif

   if (doff>-9e29) then
       do i = 1,npix_out
         if (outbuffer(i,1).gt.Healpix_undef) outbuffer(i,1)=outbuffer(i,1)-doff
      enddo
   endif

   if (fill_undef) then
      do imap = 1,nmaps
         do i = 1, npix_out
            if (outbuffer(i,imap)==Healpix_undef) outbuffer(i,imap)=0.0
         enddo
      enddo
   endif

   call fits_create(out,'!'//trim(file_out))

   allocate(columns(nmaps))
   do imap = 1,nmaps
      columns(imap) = in%columns(imap)
      columns(imap)%repcount = 1
      if (dtype=='r') then
         columns(imap)%type=FITS_REAL4
      elseif (dtype=='i') then
         columns(imap)%type=FITS_INT4
      endif
   enddo
   call fits_close(in)

   call fits_insert_bintab(out,columns)
   deallocate(columns)

   call fits_add_key(out,'PIXTYPE', 'HEALPIX','HEALPIX Pixelisation')
   call fits_add_key(out,'ORDERING',trim(ordering_out), 'Pixel ordering scheme')
   call fits_add_key(out,'NSIDE',nside_out,'Resolution parameter for HEALPIX')
   call fits_add_key(out,'FIRSTPIX',0,'First pixel # (0 based)')
   call fits_add_key(out,'LASTPIX',12*nside_out**2-1,'Last pixel # (0 based)')
   call fits_add_key(out,'POLAR',(nmaps==3),'Polarization flag')

   do imap = 1,nmaps
      chunksize = fits_fast_nelms(out,imap)
      off = 0
      do while(off.lt.npix_out)
         nout = min(chunksize,npix_out-off)

         call fits_write_column(out,imap,outbuffer(off+1:off+nout,imap),off)
         off = off+nout
      enddo
   enddo

   call fits_close(out)   

END PROGRAM
