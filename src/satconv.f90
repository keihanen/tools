PROGRAM satconv

! This tool extracts a part of a satellite pointing file

   use planck_config
   use fitsmod2

   implicit none
   integer              :: nopar, ipar, iargc
   integer              :: ifirst, ilast, i, k, n, nleft, nbuffer
   integer              :: nperiods, totperiods, totsteps, nsteps
   integer(i8b)         :: offset_read, offset_write
   real(dp)             :: t1(1), d1(1), t2(1), d2(1)
   real(dp)             :: time_offset_start, time_offset_end
   logical              :: rounding, found
   character(len=80)    :: cpar, infile, outfile
   character(len=18)    :: colnames(17)
   type(fitshandle)     :: inhandle, outhandle

   type(fitscolumn),allocatable :: columns(:)
   real(dp),allocatable :: dbuffer(:), tbuffer(:)

   infile = ''
   outfile = ''
   rounding = .false.
   ifirst = 0
   ilast = -1

   nopar = command_argument_count()

   ipar = 0
   do
      ipar = ipar+1
      if (ipar.gt.nopar) exit

      call get_command_argument(ipar,cpar)

      if (cpar(1:6)=='-round') then
         rounding = .true.
      elseif (ipar==nopar) then
         write(*,*) 'Error in arguments'
         nopar = 0
         exit
      elseif (cpar(1:3)=='-in') then
         ipar = ipar+1
         call get_command_argument(ipar,infile)
      elseif (cpar(1:4)=='-out') then
         ipar = ipar+1
         call get_command_argument(ipar,outfile)
      elseif (cpar=='-first') then
         ipar = ipar+1
         call get_command_argument(ipar,cpar)
         read(cpar,*) ifirst
      elseif (cpar=='-last') then
         ipar = ipar+1
         call get_command_argument(ipar,cpar)
         read(cpar,*) ilast
      else
         write(*,*) 'Unrecognized option: ',trim(cpar)
         nopar = 0
         exit
      endif
   enddo

   if (nopar==0) then
      write(*,*)
      write(*,*) 'Extract pointing periods [first-last] from a satellite ', &
                 'pointing file'
      write(*,*) 'Usage:'
      write(*,*) 'satconv -in satpoint_in.fits -out satpoint_out.fits'
      write(*,*) '        -first n1 [-last n2] [-round]'
      write(*,*) 'First index 0.'
      write(*,*) 'If LAST is omitted, read all periods until end of file.'
      write(*,*) '-round: Round period start/end times to nearest integer ', &
                 'second. Default=F.'
      write(*,*)
      stop
   endif

   write(*,*) 'infile       =  ',trim(infile)
   write(*,*) 'outfile      =  ',trim(outfile)
   write(*,'(x,a,i8)') 'first period = ',ifirst
   write(*,'(x,a,i8)') 'last period  = ',ilast
   write(*,'(x,a,l8)') 'rounding     = ',rounding

   if (len_trim(outfile)==0) then
      write(*,*) 'Output file name missing.'
      stop
   endif

! First HDU

   inquire(file=infile,exist=found)
   if (.not.found) then
      write(*,*) 'Input file not found.'
      stop
   endif

   call fits_open(inhandle,infile,fits_readonly)

   totperiods = inhandle%nrows*inhandle%columns(1)%repcount
   write(*,'(i6,a)') totperiods,' pointing periods found'

   if (ilast.lt.0) then
      ilast = totperiods-1
   elseif (ilast.ge.totperiods) then
      write(*,*) 'Error in arguments: last>totperiods'
      write(*,*) ilast,totperiods
      stop
   endif

   if (ifirst.gt.ilast) then
      write(*,*) 'Error in arguments: first>last'
      write(*,*) ifirst,ilast
      stop
   endif

   write(*,'(x,a,i6," -",i6)') 'Writing periods',ifirst,ilast
   nperiods = ilast-ifirst+1

   allocate(dbuffer(nperiods))
   allocate(tbuffer(nperiods))

   call fits_read_column(inhandle,1,t1)
   call fits_read_column(inhandle,2,d1)

   offset_read = ifirst
   call fits_read_column(inhandle,1,t2,offset_read)
   call fits_read_column(inhandle,2,d2,offset_read)

   time_offset_start = t2(1)-t1(1)
   time_offset_start = time_offset_start +d2(1)-d1(1)

   offset_read = ilast
   call fits_read_column(inhandle,13,t2,offset_read)
   call fits_read_column(inhandle,14,d2,offset_read)

   time_offset_end = t2(1)-t1(1)
   time_offset_end = time_offset_end +d2(1)-d1(1)

   write(*,'(x,a,f12.1)') 'time_offset_start =',time_offset_start
   write(*,'(x,a,f12.1)') 'time_offset_end   =',time_offset_end

   offset_read = ifirst
   call fits_read_column(inhandle,1,tbuffer,offset_read)
   call fits_read_column(inhandle,2,dbuffer,offset_read)

   if (rounding) then
      tbuffer = aint(tbuffer+dbuffer+0.5)
      dbuffer = 0.0
   endif

   do i = 1,17
      colnames(i) = inhandle%columns(i)%name
   enddo

!Create output file
!Write first HDU

   call fits_create(outhandle,"!"//trim(outfile))

   allocate(columns(17))
   do i = 1,17
      columns(i)%repcount = inhandle%columns(i)%repcount
      columns(i)%type = inhandle%columns(i)%type
      columns(i)%name = inhandle%columns(i)%name
      columns(i)%unit = inhandle%columns(i)%unit
   enddo

   call fits_insert_bintab(outhandle,columns)

   call fits_write_column(outhandle,1,tbuffer)
   call fits_write_column(outhandle,2,dbuffer)

   do i = 3,17
      call fits_read_column(inhandle,i,dbuffer,offset_read)
      call fits_write_column(outhandle,i,dbuffer)
   enddo

   deallocate(tbuffer,dbuffer)
   deallocate(columns)

! Second HDU

   call fits_goto_hdu(inhandle,3)
   totsteps = inhandle%nrows*inhandle%columns(1)%repcount

   offset_read = int(time_offset_start+.1)
   nsteps = int(time_offset_end-time_offset_start)

   nsteps = min(nsteps,totsteps-offset_read)

   allocate(columns(6))
   do i = 1,6
      columns(i)%name = inhandle%columns(i)%name
      columns(i)%unit = inhandle%columns(i)%unit
      columns(i)%type = inhandle%columns(i)%type
      columns(i)%repcount = inhandle%columns(i)%repcount
   enddo

   call fits_insert_bintab(outhandle,columns)

   nbuffer = 10000
   allocate(dbuffer(nbuffer))

   nleft = nsteps
   offset_write = 0
   do
      n = min(nbuffer,nleft)
      if (n==0) exit

      do i = 1,6
         call fits_read_column(inhandle,i,dbuffer(1:n),offset_read)
         call fits_write_column(outhandle,i,dbuffer(1:n),offset_write)
      enddo

      offset_read = offset_read+n
      offset_write = offset_write+n
      nleft = nleft-n
   enddo

   deallocate(dbuffer)
   deallocate(columns)

   call fits_close(inhandle)
   call fits_close(outhandle)

END PROGRAM


