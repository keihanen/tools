PROGRAM header
!
! Display the header of a fits file.
! Usage: header filename [nhdu]
! Requires the fitsio library
!
   use planck_config

   implicit none
   integer            :: blocksize, status, iargc, nopar, n
   integer            :: hdunum=0, nhdu, hdutype, key_no, keysexist
   character(len=80)  :: card, chdu
   character(len=200) :: filename

   nopar = command_argument_count()

   n = 0
   if (nopar==0) then
      write(*,*) 'Usage: header filename [nhdu]'
      stop
   elseif (nopar==1) then
      call get_command_argument(1,filename)
   else
      call get_command_argument(1,filename)
      call get_command_argument(2,chdu)

      read(chdu,*) n
   endif

   status=0
   call FTOPEN(10,filename,0,blocksize,status)
   call FTTHDU(10,hdunum,status)
   call errorstatus(status,'FTOPEN/FTTHDU')

   do nhdu = 1,hdunum
      if (n.gt.0.and.n.ne.nhdu) cycle

      write(*,*)
      write(*,*) '-- Header unit no ',nhdu,' --'

      call FTMAHD(10,nhdu,hdutype,status)
      call FTGHPS(10,keysexist,key_no,status)
      call errorstatus(status,'FTMAHD/FTGHPS')

      do key_no = 1, keysexist+1
         call FTGREC(10,key_no,card,status)
         call errorstatus(status,'FTGREC')
         write(*,'(a80)') card
      enddo

   enddo
   call FTCLOS(10,status)

CONTAINS

   SUBROUTINE errorstatus(status,routine)
      integer           :: status
      character(len=*)  :: routine
      character(len=30) :: errtext

      if (status.gt.0) then
         call ftgerr(status,errtext)
         write(*,*) 'Error in ',routine
         write(*,*) errtext
         stop
      endif

    END SUBROUTINE

END PROGRAM header
