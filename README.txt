This package contains miscellaneous tools for handling of fits files etc.

**********
HEADER
displays the header of a fits file.

Syntax:
./header file.fits [nhdu]

nhdu defines the HDU. Default is 2.


**********
FSHOW
displays the content of a binary fitsio file.

Syntax: 
./fshow filename -options

Options:
-n      n       Number of rows (default: show all)
-offset offset  Rows omitted (default=0)
-col    col     Column index (default: show all). First column index is 1.
                You can give a single column index (for example -col 1)
                or specify a range (for example -col 1-3)
-hdu    nhdu    HDU (default: 2)
-dig    n       Number of digits displayed
-lines          Show line numbers
-dat            Do not show header information


***********
SATCONV
extracts a section of a satellite pointing file.

Syntax:
./satconv  -in  satpoint_in.fits      input file
           -out satpoint_out.fits     output file
           -first n1                  first pointing period (indexing starts at 0)
          [-last n2]                  last pointing period
          [-round]                  round start/end time of periods to nearest integer second


**********
SAT2DET
converts satellite pointing into detector pointing.

Syntax:
./sat2det -in satpoint_in.fits
          -out detpoint.fits
          -theta theta  (deg)  theta,phi,psi define detector's position on focal plane
          -phi   phi    (deg)
          -psi   psi    (deg)
         [-time(_start) t] (s) starting time, default=0
         [-nosamples    n] number of samples
         [-first       n1] first pointing period (indexing starts from 0)
         [-last        n2] last pointing period
         [-opening      a] (deg) opening angle, default=85.0
The user should provide either time_start and nosamples, or first and last.
If both are omitted, conversion is done for the whole duration.


***********
FINDMEAN
finds the mean of a Healpix map.

Syntax: findmean map.fits [frac]
The fraction FRAC of pixels with highest values are masked out.
This is useful when one wants to know the monopole of the CMB component
in a map which contains foregrounds. Default is frac=0.1.


***********
STDMAP
computes the std of a Healpix map, or,if two file names are given, 
the std of the difference.
Optionally the code writes out the difference map.
Optionally applies a mask before computing the std.
By default, all colums are read.
If a column index is given, computation is limited to one column.
Indexing starts from 1.

Syntax:
stdmap file1.fits [file2.fits -out diff.fits -mask mask.fits -col i]'


*************
PIXEL_CENTERS
finds the centers of Healpix pixels in nested pixeling and writes them out as a Healpix map.

Syntax:
./pixel_centers nside


*************
MODIFYMAP
transforms a Healpix map from RING pixeling to NESTED, or vice versa,
and performs up/downgrading. 
Optionally removes the monopole or fill undefined pixels by zero.

Syntax:
./modifymap inmap.fits outmap.fits [-nside N -ordering ring/nested 
-fill_undef -rm_monopole -rm_offset x -add_offset x]

-nside:       resolution of the target map. Default=input resolution
-ordering:    pixel ordering scheme for the target map. Dfault=same as the input map
-fill_undef:  Flag: Replace Healpix_undef by zero
-rm_monopole: Flag: Determine the monopoel of the map and subtract it (T nly)
-rm_offset:   subtract given value from T map
-add_offset:  add given value to T map
-mask:      Mask to be applied to the input map before down/upgrading.
                 The mask must have same resolution as the input map,
                  but ordering scheme (ring/nested) is free,
                   


*************
CREATE_MASK
Given as input a temperature map, this tool builds a sky mask.
Pixels with temperature above the treshold are marked by value 1, others by 0.
Optionally the code also changes the pixeling scheme between ring/nested
 and up/downgrades the mask to given resolution.
The code can also be used to up/downgrade of an existing mask.

Syntax:
./create_mask inmap.fits out.fits [-key value]

inmap.fits: Input map. Can have integer or real values.
out.fits:   Output (integer) mask.
Options:
-col:       Column read. Default=1.
-tr:        Treshold value for pixels to be included in the output mask.
            Values greater than this are accepted (equal values are not). Default=0.0.
-nside:     Output resolution. Default=input resolution.
-ordering:  Ordering of the output map, ring/nested. Default=ordering of the input map.
-mode:      Downgrading mode (only relevant if the mask is downgraded)
              1 = only accept pixels with all subpixels above the treshold (default)
              2 = accept pixels with any subpixel above the treshold
-rev:       Reverse the mask, i.e. accept only pixels below or equal to the treshold. Default=F.


**************
COMBINEMAPS
creates a linear combination of fitsio files.

Syntax:
./combinemaps file1.fits weight1 file2.fits weight2... file_out.fits
Files must be of equal size, shape, and type.


*************
FACTORIZE
finds the prime factors of given integer.

Syntax: 
factorize n


*************
BITS
Bit presentation of an integer

Syntax:
bits n


*************
MAPPROFILE
computes the "profile" of a Healpix map, i.e. integrated temperature
as a function of latitude, starting from the north pole,
normalized so that the last value is the average value of the map.

Syntax:
mapprofile map.fits profile.dat


*************
FITS2DAT
reads a fits file and writes its contents into an ascii file.
Output file name is the same as the input file name, with ".dat" appended.

Syntax:
fits2dat file.fits


************
DAT2FITS
reads an ascii file and writes its contents into a fits file.
Output file name is the same as the input file name, with ".fits" appended.

Syntax:
dat2fits file.dat [XX -bin]
XX is a string which defines the column data types, for example "JDE".
Default is D (one double precision column.
If -bin is given, data is written into a binary table instead os ascii.

************
MAP_HIST
displays a histogram of the contents of a fits file.

Syntax: map_hist filename [-col N] [-bin X] [-log/-lin]
 -col N: column index  (default 1)
 -log:   logarithmic scale (default)
 -lin:   linear scale
 -bin X: bin width.
 If logarithmic scale is selected, code displays negative and positive values separately.
   Bin is then the step in log_10 (default 1.0).'

************
HIT_HIST
Displays a histogram of an integer-valued fits column.

Syntax: hit_hist filename [-col N]
 -col: column index  (default 1)'

**************
COMBINE_SUMMAPS
constructs I,Q,U maps from a set of sum maps and pixel matrices.
Allows to degrade the resolution.

Syntax: combine_summaps params.par
Input parameters:
  nside            = Resolution parameter. Default=resolution of the input maps
  nstokes          = Number of Stokes components, 1 or 3. Default=3
  nmaps            = Number of input maps.
  file_summap_N    = Nth input sum map (FITS). N=1,2,...
  file_matrix_N    = Nth input matrix (FITS).
  weight_N         = Relative weight for Nth map. Default=1.
  file_outmap      = Output map name. (FITS file)


************
POINTING_COMPACTOR
rewrites a detector pointing file in compact form.

Syntax: pointing_compactor infile.fits outfile.fits
Theta,phi are rotated to galactic frame and converted to a nested pixel number
at resolution nside=4096. Psi angle is stored as an integer.
The compacted pointing takes 8 bytes (2 integers) per sample, while the original pointing
takes 24 bytes (3 doubles). Madam (version 3.7.4) can read the compacted pointing.


************
plotdiff.sh (Valtteri Lindholm)
plots the smoothed difference of two Healpix maps

Syntax: plotdiff map1.fits map2.fits [-fwhm -min -max]"
Default smoothing fwhm=60 (arcmin)
